--DROP DATABASE marveldb;
CREATE DATABASE marveldb;
CREATE USER stanlee WITH PASSWORD 'stanlee';
GRANT ALL PRIVILEGES ON DATABASE marveldb TO stanlee;

-- Table: public.characters

-- DROP TABLE IF EXISTS public.characters CASCADE;

CREATE TABLE public.characters
(
    character_id SERIAL NOT NULL,
    name character varying,
    description character varying,
    modified date,
    thumbnail_name character varying,
    CONSTRAINT character_pkey PRIMARY KEY (character_id)
);
ALTER TABLE public.characters OWNER to stanlee;
    
    -- Table: public.comic

-- DROP TABLE IF EXISTS public.comics CASCADE;

CREATE TABLE public.comics
(
    comic_id SERIAL NOT NULL,
    title character varying,
    description character varying,
    start_date date,
    modified date,
    page_count integer NOT NULL,
    price integer NOT NULL,
    thumbnail_name character varying,
    CONSTRAINT comics_pkey PRIMARY KEY (comic_id)
);
ALTER TABLE public.comics OWNER to stanlee;
    
-- Table: public.comics_characters

-- DROP TABLE public.comics_characters CASCADE;

CREATE TABLE public.comics_characters
(
    comic_id integer NOT NULL,
    character_id integer NOT NULL,
    CONSTRAINT comic_character UNIQUE(comic_id, character_id),
    CONSTRAINT fkou5k4erur3y4liwpug3bjilxa FOREIGN KEY (comic_id)
        REFERENCES public.comics (comic_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkq4l4weyxchc95e0wie2uivgp4 FOREIGN KEY (character_id)
        REFERENCES public.characters (character_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
ALTER TABLE public.comics_characters OWNER to stanlee;