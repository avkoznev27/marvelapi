package com.mymarvel.marvelapi.utils.mapper;

import static org.springframework.util.Assert.notNull;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.mymarvel.marvelapi.model.dto.CharacterList;
import com.mymarvel.marvelapi.model.dto.CharacterSummary;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;
import com.mymarvel.marvelapi.repository.CharacterRepository;
import com.mymarvel.marvelapi.utils.ResourceUriBuilder;

@Component
public class ComicMapper {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ComicMapper.class);
    
    private ModelMapper modelMapper;
    private ResourceUriBuilder uriBuilder;
    private CharacterRepository characterRepository;

    public ComicMapper(ModelMapper modelMapper, 
                       ResourceUriBuilder uriBuilder, 
                       CharacterRepository characterRepository) {
        this.modelMapper = modelMapper;
        this.uriBuilder = uriBuilder;
        this.characterRepository = characterRepository;
    }

    public Comic toEntity(ComicDto comicDto) {
        LOGGER.debug("Converting {} to entity", comicDto);
        notNull(comicDto, "ComicDTO cannot be null");
        Comic comic = modelMapper.map(comicDto, Comic.class);
        comic.setCharacters(new HashSet<>(characterRepository.findAllById(comicDto.getCharacterIds())));
        return comic;
    }
    
    public ComicDto toDto(Comic comic) {
        LOGGER.debug("Converting entity comic to dto", comic);
        notNull(comic, "Character cannot be null");
        ComicDto comicDto = modelMapper.map(comic, ComicDto.class);
        comicDto.setResourceURI(uriBuilder.getComicUri(comic));
        comicDto.setCharacterList(createCharacterList(comic));
        comicDto.setCharacterIds(comic.getCharacters().stream()
                .mapToInt(Character::getCharacterId)
                .boxed()
                .collect(Collectors.toList()));
        LOGGER.debug("Converted comic dto {}", comicDto);
        return comicDto;
    }

    private CharacterList createCharacterList(Comic comic) {
       CharacterList characterList = new CharacterList();
       List<CharacterSummary> items = comic.getCharacters().stream()
               .sorted()
               .map(character -> {
           CharacterSummary item = new CharacterSummary();
           item.setName(character.getName());
           item.setResourceURI(uriBuilder.getCharacterUri(character));
           return item;
       }).collect(Collectors.toList());
       characterList.setAvailable(items.size());
       characterList.setCollectionURI(uriBuilder.getComicCharactersUri(comic));
       characterList.setItems(items);
       return characterList;
    }
}
