package com.mymarvel.marvelapi.utils.mapper;

import static org.springframework.util.Assert.notNull;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicList;
import com.mymarvel.marvelapi.model.dto.ComicSummary;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;
import com.mymarvel.marvelapi.repository.ComicRepository;
import com.mymarvel.marvelapi.utils.ResourceUriBuilder;

@Component
public class CharacterMapper {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CharacterMapper.class);

    private ModelMapper modelMapper;
    private ResourceUriBuilder uriBuilder;
    private ComicRepository comicRepository;
    
    public CharacterMapper(ModelMapper modelMapper, 
                           ResourceUriBuilder uriBuilder, 
                           ComicRepository comicRepository) {
        this.modelMapper = modelMapper;
        this.uriBuilder = uriBuilder;
        this.comicRepository = comicRepository;
    }

    public Character toEntity(CharacterDto characterDto) {
        LOGGER.debug("Converting characterDto object to entity, object {}", characterDto);
        notNull(characterDto, "CharacterDTO cannot be null");
        Character character = modelMapper.map(characterDto, Character.class);
        character.setComics(new HashSet<>(comicRepository.findAllById(characterDto.getComicIds())));
        return character;
    }

    public CharacterDto toDto(Character character) {
        LOGGER.debug("Converting character to dto {}", character);
        notNull(character, "Character cannot be null");
        CharacterDto characterDto = modelMapper.map(character, CharacterDto.class);
        characterDto.setResourceURI(uriBuilder.getCharacterUri(character));
        characterDto.setComicList(createComicList(character));
        characterDto.setComicIds(character.getComics().stream()
                .mapToInt(Comic::getComicId)
                .boxed()
                .collect(Collectors.toList()));
        LOGGER.debug("Converted character dto {}", characterDto);
        return characterDto;
    }

    private ComicList createComicList(Character character) {
        ComicList comicList = new ComicList();
        List<ComicSummary> items = character.getComics().stream()
                .sorted()
                .map(comic -> {
            ComicSummary item = new ComicSummary();
            item.setTitle(comic.getTitle());
            item.setResourceURI(uriBuilder.getComicUri(comic));
            return item;
        }).collect(Collectors.toList());
        comicList.setAvailable(items.size());
        comicList.setCollectionURI(uriBuilder.getCharacterComicsUri(character));
        comicList.setItems(items);
        return comicList;
    }
}
