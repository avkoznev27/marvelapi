package com.mymarvel.marvelapi.utils;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentContextPath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.springframework.util.Assert.notNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;

@Component
public class ResourceUriBuilder {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceUriBuilder.class);
    
    @Value("${api.characters.base.path}")
    private String charactersApiBasePath;
    @Value("${api.comics.base.path}")
    private String comicsApiBasePath;
    
    public String getCharacterUri(Character character) {
        notNull(character, "Character must not be null");
        LOGGER.debug("Building resourceUri for character with id = {}", character.getCharacterId());
        return fromCurrentContextPath()
                .path(charactersApiBasePath)
                .pathSegment(String.valueOf(character.getCharacterId()))
                .toUriString();
    }
    
    public String getCharacterComicsUri(Character character) {
        notNull(character, "Character must not be null");
        LOGGER.debug("Building resourceUri for comics character with id = {}", character.getCharacterId());
        return fromCurrentContextPath()
                .path(charactersApiBasePath)
                .pathSegment(String.valueOf(character.getCharacterId()), "comics")
                .toUriString();
    }
    
    public String getComicUri(Comic comic) {
        notNull(comic, "Comic must not be null");
        LOGGER.debug("Building resourceUri for comic with id = {}", comic.getComicId());
        return fromCurrentContextPath()
                .path(comicsApiBasePath)
                .pathSegment(String.valueOf(comic.getComicId()))
                .toUriString();
    }
    
    public String getComicCharactersUri(Comic comic) {
        notNull(comic, "Comic must not be null");
        LOGGER.debug("Building resourceUri for characters comic with id = {}", comic.getComicId());
        return fromCurrentContextPath()
                .path(comicsApiBasePath)
                .pathSegment(String.valueOf(comic.getComicId()), "characters")
                .toUriString();
    }

}
