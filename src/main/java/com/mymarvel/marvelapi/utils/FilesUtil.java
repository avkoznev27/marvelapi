package com.mymarvel.marvelapi.utils;

import static org.springframework.util.Assert.notNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.mymarvel.marvelapi.exceptions.ResourceNotFoundException;
import com.mymarvel.marvelapi.exceptions.UploadFileException;

@Component
public class FilesUtil {
    
    @Value("${upload.path}")
    private String uploadDir;
    
    public String uploadFile(MultipartFile file) {
        notNull(file, "File must not be null");
        Path uploadPath = Paths.get(uploadDir).toAbsolutePath().normalize();
        createUploadDir(uploadPath);
        String fileName = UUID.randomUUID().toString() + "-" + file.getOriginalFilename();
        try(InputStream inputStream = file.getInputStream()) {
            checkFileMediaType(file);
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new UploadFileException(e.getMessage());
        }
        return fileName;
    }
    
    private void checkFileMediaType(MultipartFile file) throws IOException {
        String mimeType = new Tika().detect(file.getBytes());
        if (!mimeType.contains("image")) {
            throw new UploadFileException(
                    String.format("The file must be an image. Media type of current file: %s", mimeType));
        }  
    }

    private void createUploadDir(Path uploadPath) {
        if (!Files.exists(uploadPath)) {
            try {
                Files.createDirectories(uploadPath);
            } catch (IOException e) {
                throw new UploadFileException("Cannot create directory " + e.getMessage());
            }
        }
    }

    public Resource loadFileAsResource(String fileName) {
        notNull(fileName, "File name must not be null");
        try {
            Path filePath = Paths.get(uploadDir).resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new ResourceNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new ResourceNotFoundException(ex.getMessage());
        }
    }
}
