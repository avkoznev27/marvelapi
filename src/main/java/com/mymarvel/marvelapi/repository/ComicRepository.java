package com.mymarvel.marvelapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.mymarvel.marvelapi.model.entities.Comic;

@Repository
public interface ComicRepository extends JpaRepository<Comic, Integer>, JpaSpecificationExecutor<Comic>{

}
