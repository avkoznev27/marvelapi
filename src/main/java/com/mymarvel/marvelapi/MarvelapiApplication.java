package com.mymarvel.marvelapi;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@SpringBootApplication
public class MarvelapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarvelapiApplication.class, args);
	}
	
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI().info(new Info().title("Marvel Api").version("1.0.0"));
    }
    
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
