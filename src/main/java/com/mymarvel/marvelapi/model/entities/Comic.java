package com.mymarvel.marvelapi.model.entities;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "comics")
public class Comic implements Comparable<Comic> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int comicId;
    private String title;
    private String description;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate modified;
    private int pageCount;
    private int price;
    private String thumbnailName;
    @ManyToMany
    @JoinTable(name = "comics_characters", 
            joinColumns = { @JoinColumn(name = "comic_id") }, 
            inverseJoinColumns = { @JoinColumn(name = "character_id")})
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<Character> characters;

    public Comic() {
        this.characters = new HashSet<>();
    }
    
    public Comic(int comicId) {
        this();
        this.comicId = comicId;
    }

    public int getComicId() {
        return comicId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getModified() {
        return modified;
    }

    public int getPageCount() {
        return pageCount;
    }

    public int getPrice() {
        return price;
    }

    public String getThumbnailName() {
        return thumbnailName;
    }

    public Set<Character> getCharacters() {
        return characters;
    }

    public void setComicId(int id) {
        this.comicId = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setModified(LocalDate modified) {
        this.modified = modified;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setThumbnailName(String thumbnailName) {
        this.thumbnailName = thumbnailName;
    }

    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + comicId;
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + pageCount;
        result = prime * result + price;
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((thumbnailName == null) ? 0 : thumbnailName.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Comic other = (Comic) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (comicId != other.comicId)
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (pageCount != other.pageCount)
            return false;
        if (price != other.price)
            return false;
        if (startDate == null) {
            if (other.startDate != null)
                return false;
        } else if (!startDate.equals(other.startDate))
            return false;
        if (thumbnailName == null) {
            if (other.thumbnailName != null)
                return false;
        } else if (!thumbnailName.equals(other.thumbnailName))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Comic [id=" + comicId 
                + ", title=" + title 
                + ", description=" + description 
                + ", startDate=" + startDate
                + ", modified=" + modified 
                + ", pageCount=" + pageCount 
                + ", price=" + price 
                + ", thumbnailName=" + thumbnailName + "]";
    }

    @Override
    public int compareTo(Comic o) {
        return title.compareTo(o.getTitle());
    }
}
