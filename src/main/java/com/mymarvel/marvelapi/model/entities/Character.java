package com.mymarvel.marvelapi.model.entities;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "characters")
public class Character implements Comparable<Character> {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int characterId;
    private String name;
    private String description;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate modified;
    private String thumbnailName;
    @ManyToMany(mappedBy = "characters")
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<Comic> comics;
    
    public Character() {
        this.comics = new HashSet<>();
    }
    
    public Character(int characterId) {
        this();
        this.characterId = characterId;
    }

    public int getCharacterId() {
        return characterId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getModified() {
        return modified;
    }

    public String getThumbnailName() {
        return thumbnailName;
    }
    
    public Set<Comic> getComics() {
        return comics;
    }

    public void setCharacterId(int id) {
        this.characterId = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setModified(LocalDate modified) {
        this.modified = modified;
    }

    public void setThumbnailName(String thumbnailName) {
        this.thumbnailName = thumbnailName;
    }
    
    public void setComics(Set<Comic> comics) {
        this.comics = comics;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + characterId;
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((thumbnailName == null) ? 0 : thumbnailName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Character other = (Character) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (characterId != other.characterId)
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (thumbnailName == null) {
            if (other.thumbnailName != null)
                return false;
        } else if (!thumbnailName.equals(other.thumbnailName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Character [id=" + characterId 
                + ", name=" + name 
                + ", description=" + description 
                + ", modified=" + modified
                + ", thumbnailName=" + thumbnailName + "]";
    }

    @Override
    public int compareTo(Character o) {
        return name.compareTo(o.getName());
    }
}
