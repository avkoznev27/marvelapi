package com.mymarvel.marvelapi.model.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

public class CharacterList implements Serializable {

    private static final long serialVersionUID = -1628448271297748063L;
    
    @Schema(description = "The number of total available characters in this list.", example = "20")
    private int available;
    @Schema(description = "The path to the full list of characters in this collection", 
            example = "http://localhost:8080/v1/public/comics/1/characters")
    private String collectionURI;
    @Schema(description = "The list of returned characters in this collection.")
    private List<CharacterSummary> items;
    
    public int getAvailable() {
        return available;
    }
    
    public String getCollectionURI() {
        return collectionURI;
    }
    
    public List<CharacterSummary> getItems() {
        return items;
    }
    
    public void setAvailable(int available) {
        this.available = available;
    }
    
    public void setCollectionURI(String collectionURI) {
        this.collectionURI = collectionURI;
    }
    
    public void setItems(List<CharacterSummary> items) {
        this.items = items;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + available;
        result = prime * result + ((collectionURI == null) ? 0 : collectionURI.hashCode());
        result = prime * result + ((items == null) ? 0 : items.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CharacterList other = (CharacterList) obj;
        if (available != other.available)
            return false;
        if (collectionURI == null) {
            if (other.collectionURI != null)
                return false;
        } else if (!collectionURI.equals(other.collectionURI))
            return false;
        if (items == null) {
            if (other.items != null)
                return false;
        } else if (!items.equals(other.items))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CharacterList [available=" + available 
                + ", collectionURI=" + collectionURI 
                + ", items=" + items + "]";
    }
}
