package com.mymarvel.marvelapi.model.dto;

import java.io.Serializable;

import io.swagger.v3.oas.annotations.media.Schema;

public class ComicSummary implements Serializable {

    private static final long serialVersionUID = -1654847334738808014L;
    
    @Schema(description = "The path to the individual comic resource.", example = "http://localhost:8080/v1/public/comics/1")
    private String resourceURI;
    @Schema(description = "The canonical name of the comic", example = "X-men")
    private String title;
    
    public String getResourceURI() {
        return resourceURI;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((resourceURI == null) ? 0 : resourceURI.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ComicSummary other = (ComicSummary) obj;
        if (resourceURI == null) {
            if (other.resourceURI != null)
                return false;
        } else if (!resourceURI.equals(other.resourceURI))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ComicSummary [resourceURI=" + resourceURI + ", title=" + title + "]";
    }
}
