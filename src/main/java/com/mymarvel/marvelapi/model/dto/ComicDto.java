package com.mymarvel.marvelapi.model.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Schema(name = "Comic")
public class ComicDto implements Serializable, Comparable<ComicDto> {
    
    private static final long serialVersionUID = 88623131637920516L;
    
    @PositiveOrZero
    @Schema(description = "The unique ID of the comic resource.")
    private int id;
    @NotBlank
    @Schema(description = "The canonical title of the comic.", example = "Iron-Man")
    private String title;
    @Schema(description = "The preferred description of the comic.", example = "Description")
    private String description;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Schema(description = "The comic start date")
    private LocalDate startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Schema(description = "The date the resource was most recently modified.", accessMode = AccessMode.READ_ONLY)
    private LocalDate modified;
    @Schema(description = "The number of story pages in the comic.", example = "1000")
    private int pageCount;
    @Schema(description = "Comic price", example = "10")
    private int price;
    @Schema(description = "The canonical URL identifier for this resource.", 
            example = "http://localhost:8080/v1/public/comics/1", accessMode = AccessMode.READ_ONLY)
    private String resourceURI;
    @Schema(description = "The name of representative image for this comic.", 
            example = "image.jpg", accessMode = AccessMode.READ_ONLY)
    private String thumbnailName;
    @Schema(description = " A resource list containing the characters which appear in this comic.", 
            accessMode = AccessMode.READ_ONLY)
    private CharacterList characterList;
    
    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(description = "List of character ids for comic", example = "[1, 10, 20]")
    private List<Integer> characterIds;
    
    public ComicDto() {
        this.characterIds = new ArrayList<>();
    }

    public int getId() {
        return id;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getDescription() {
        return description;
    }
    
    public LocalDate getStartDate() {
        return startDate;
    }
    
    public LocalDate getModified() {
        return modified;
    }
    
    public int getPageCount() {
        return pageCount;
    }
    
    public int getPrice() {
        return price;
    }
    
    public String getResourceURI() {
        return resourceURI;
    }
    
    public String getThumbnailName() {
        return thumbnailName;
    }
    
    public CharacterList getCharacterList() {
        return characterList;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }
    
    public void setModified(LocalDate modified) {
        this.modified = modified;
    }
    
    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }
    
    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }
    
    public void setThumbnailName(String thumbnailName) {
        this.thumbnailName = thumbnailName;
    }
    
    public void setCharacterList(CharacterList characterList) {
        this.characterList = characterList;
    }

    public List<Integer> getCharacterIds() {
        return characterIds;
    }

    public void setCharacterIds(List<Integer> characterIds) {
        this.characterIds = characterIds;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + id;
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + pageCount;
        result = prime * result + price;
        result = prime * result + ((resourceURI == null) ? 0 : resourceURI.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((thumbnailName == null) ? 0 : thumbnailName.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ComicDto other = (ComicDto) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (id != other.id)
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (pageCount != other.pageCount)
            return false;
        if (price != other.price)
            return false;
        if (resourceURI == null) {
            if (other.resourceURI != null)
                return false;
        } else if (!resourceURI.equals(other.resourceURI))
            return false;
        if (startDate == null) {
            if (other.startDate != null)
                return false;
        } else if (!startDate.equals(other.startDate))
            return false;
        if (thumbnailName == null) {
            if (other.thumbnailName != null)
                return false;
        } else if (!thumbnailName.equals(other.thumbnailName))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ComicDto [id=" + id 
                + ", title=" + title 
                + ", description=" + description 
                + ", startDate=" + startDate
                + ", modified=" + modified 
                + ", pageCount=" + pageCount 
                + ", price=" + price 
                + ", resourceURI=" + resourceURI 
                + ", thumbnailName=" + thumbnailName
                + ", characterIds=" + characterIds
                + ", characterList=" + characterList + "]";
    }

    @Override
    public int compareTo(ComicDto o) {
        return title.compareTo(o.getTitle());
    }
}
