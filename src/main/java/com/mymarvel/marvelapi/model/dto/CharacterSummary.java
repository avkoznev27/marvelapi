package com.mymarvel.marvelapi.model.dto;

import java.io.Serializable;

import io.swagger.v3.oas.annotations.media.Schema;

public class CharacterSummary implements Serializable {
    
    private static final long serialVersionUID = 2602263868426213510L;
    
    @Schema(description = "The path to the individual character resource.", 
            example = "http://localhost:8080/v1/public/characters/1")
    private String resourceURI;
    @Schema(description = "The full name of the character.", example = "Superman")
    private String name;
    
    public String getResourceURI() {
        return resourceURI;
    }
    
    public String getName() {
        return name;
    }
    
    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((resourceURI == null) ? 0 : resourceURI.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CharacterSummary other = (CharacterSummary) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (resourceURI == null) {
            if (other.resourceURI != null)
                return false;
        } else if (!resourceURI.equals(other.resourceURI))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CharacterSummary [resourceURI=" + resourceURI + ", name=" + name + "]";
    }
}
