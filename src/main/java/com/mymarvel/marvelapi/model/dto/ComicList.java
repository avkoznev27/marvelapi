package com.mymarvel.marvelapi.model.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

public class ComicList implements Serializable {

    private static final long serialVersionUID = 3176859835609585779L;
    
    @Schema(description = "The number of total available issues in this list.", example = "30")
    private int available;
    @Schema(description = "The path to the full list of issues in this collection.", 
            example = "http://localhost:8080/v1/public/characters/1/comics")
    private String collectionURI;
    @Schema(description = "The list of returned issues in this collection.")
    private List<ComicSummary> items;
    
    public int getAvailable() {
        return available;
    }
    
    public String getCollectionURI() {
        return collectionURI;
    }
    
    public List<ComicSummary> getItems() {
        return items;
    }
    
    public void setAvailable(int available) {
        this.available = available;
    }
    
    public void setCollectionURI(String resourceURI) {
        this.collectionURI = resourceURI;
    }
    
    public void setItems(List<ComicSummary> items) {
        this.items = items;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + available;
        result = prime * result + ((collectionURI == null) ? 0 : collectionURI.hashCode());
        result = prime * result + ((items == null) ? 0 : items.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ComicList other = (ComicList) obj;
        if (available != other.available)
            return false;
        if (collectionURI == null) {
            if (other.collectionURI != null)
                return false;
        } else if (!collectionURI.equals(other.collectionURI))
            return false;
        if (items == null) {
            if (other.items != null)
                return false;
        } else if (!items.equals(other.items))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ComicList [available=" + available 
                + ", collectionURI=" + collectionURI 
                + ", items=" + items + "]";
    }
}
