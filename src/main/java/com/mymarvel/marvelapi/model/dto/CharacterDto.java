package com.mymarvel.marvelapi.model.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Schema(name = "Character")
public class CharacterDto implements Serializable, Comparable<CharacterDto> {
    
    private static final long serialVersionUID = -1019355593553989037L;
    
    @PositiveOrZero
    @Schema(description = "The unique ID of the character resource.")
    private int id;
    @NotBlank
    @Schema(description = "The name of the character.", example = "Spider man")
    private String name;
    @Schema(description = "A short bio or description of the character.", example = "Payk")
    private String description;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Schema(description = "The date the resource was most recently modified.", accessMode = AccessMode.READ_ONLY)
    private LocalDate modified;
    @Schema(description = "The canonical URL identifier for this resource.", 
            example = "http://localhost:8080/v1/public/characters/42", accessMode = AccessMode.READ_ONLY)
    private String resourceURI;
    @Schema(description = "The representative image for this character.", 
            example = "image.jpg", accessMode = AccessMode.READ_ONLY)
    private String thumbnailName;
    @Schema(description = "A resource list containing comics which feature this character.", 
            accessMode = AccessMode.READ_ONLY)
    private ComicList comicList;
    
    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(description = "List of comic ids for character", example = "[1, 2, 3]", hidden = true)
    private List<Integer> comicIds;
    
    public CharacterDto() {
        this.comicIds = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getModified() {
        return modified;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public String getThumbnailName() {
        return thumbnailName;
    }

    public ComicList getComicList() {
        return comicList;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setModified(LocalDate modified) {
        this.modified = modified;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public void setThumbnailName(String thumbnailName) {
        this.thumbnailName = thumbnailName;
    }

    public void setComicList(ComicList comicList) {
        this.comicList = comicList;
    }

    public List<Integer> getComicIds() {
        return comicIds;
    }

    public void setComicIds(List<Integer> comicIds) {
        this.comicIds = comicIds;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + id;
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((resourceURI == null) ? 0 : resourceURI.hashCode());
        result = prime * result + ((thumbnailName == null) ? 0 : thumbnailName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CharacterDto other = (CharacterDto) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (id != other.id)
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (resourceURI == null) {
            if (other.resourceURI != null)
                return false;
        } else if (!resourceURI.equals(other.resourceURI))
            return false;
        if (thumbnailName == null) {
            if (other.thumbnailName != null)
                return false;
        } else if (!thumbnailName.equals(other.thumbnailName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CharacterDto [id=" + id 
                + ", name=" + name 
                + ", description=" + description 
                + ", modified=" + modified
                + ", resourceURI=" + resourceURI 
                + ", thumbnailName=" + thumbnailName 
                + ", comicIds=" + comicIds
                + ", comicList=" + comicList + "]";
    }

    @Override
    public int compareTo(CharacterDto o) {
        return name.compareTo(o.getName());
    }
}
