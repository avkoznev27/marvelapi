package com.mymarvel.marvelapi.exceptions;

public class UploadFileException extends RuntimeException {

    private static final long serialVersionUID = -3491196090920555198L;

    public UploadFileException() {
        super();
    }

    public UploadFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public UploadFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public UploadFileException(String message) {
        super(message);
    }

    public UploadFileException(Throwable cause) {
        super(cause);
    }
}
