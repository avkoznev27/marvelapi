package com.mymarvel.marvelapi.exceptions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MarvelApiError {
    
    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String exception;
    private String message;
    private List<ValidationError> subErrors;

    private MarvelApiError() {
        timestamp = LocalDateTime.now();
    }

    public MarvelApiError(HttpStatus status) {
        this();
        this.status = status;
    }

    public MarvelApiError(HttpStatus status, Throwable ex) {
        this();
        this.status = status;
        this.exception = ex.getClass().getSimpleName();
        this.message = ex.getLocalizedMessage();
    }
    
    public void addFieldValidationErrors(List<FieldError> fieldErrors) {
        fieldErrors.forEach(this::addValidationError);
    }
    
    public void addGlobalValidationErrors(List<ObjectError> globalErrors) {
        globalErrors.forEach(this::addValidationError);
    }
    
    public void addValidationErrors(Set<ConstraintViolation<?>> constraintViolations) {
        constraintViolations.forEach(this::addValidationError);
    }
    
    private void addSubError(ValidationError subError) {
        if (subErrors == null) {
            subErrors = new ArrayList<>();
        }
        subErrors.add(subError);
    }

    private void addValidationError(FieldError fieldError) {
        addSubError(new ValidationError(
                fieldError.getObjectName(),
                fieldError.getField(),
                fieldError.getRejectedValue(),
                fieldError.getDefaultMessage()));
    }

    private void addValidationError(ObjectError objectError) {
        addSubError(new ValidationError(
                objectError.getObjectName(),
                objectError.getDefaultMessage()));
    }

    private void addValidationError(ConstraintViolation<?> cv) {
        addSubError(new ValidationError(
                cv.getRootBeanClass().getSimpleName(),
                ((PathImpl) cv.getPropertyPath()).getLeafNode().asString(),
                cv.getInvalidValue(),
                cv.getMessage()));
    }

    public HttpStatus getStatus() {
        return status;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public List<ValidationError> getSubErrors() {
        return subErrors;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setSubErrors(List<ValidationError> subErrors) {
        this.subErrors = subErrors;
    }

    public String getException() {
        return exception;
    }

    public String getMessage() {
        return message;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public void setMessage(String message) {
        this.message = message;
    } 
}
