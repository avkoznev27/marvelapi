package com.mymarvel.marvelapi.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.service.CharacterService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("${api.characters.base.path}")
@Tag(name = "Characters")
public class CharactersController {

    private CharacterService characterService;

    @Autowired
    public CharactersController(CharacterService characterService) {
        this.characterService = characterService;
    }

    @GetMapping
    @Operation(summary = "Fetches lists of characters.")
    public Page<CharacterDto> getCharacters(@ParameterObject CharacterFilterParameter parameters,
                                            @Parameter(description = "Return only characters which appear in "
                                                    + "the specified comics (accepts a comma-separated list of ids)") 
                                            @RequestParam(required = false, name = "comics") List<Integer> comicIds,
                                            @PageableDefault(sort = "characterId", direction = Sort.Direction.ASC, size = 10)
                                            @ParameterObject Pageable pageable) {
        return characterService.findAllCharacters(parameters, comicIds, pageable);
    }

    @GetMapping("/{characterId}")
    @Operation(summary = "Fetches a single character by id.")
    public CharacterDto getCharacterById(@PathVariable("characterId") int characterId) {
        return characterService.findCharacterById(characterId);
    }

    @GetMapping("/{characterId}/comics")
    @Operation(summary = "Fetches lists of comics filtered by a character id.")
    public Page<ComicDto> getComicsByCharacterId(@PathVariable("characterId") int characterId,
                                             @ParameterObject @Valid ComicFilterParameter comicsParameters, 
                                             @ParameterObject Pageable pageable) {
        return characterService.findComicsByCharacterId(characterId, comicsParameters, pageable);
    }
    
    @PostMapping(value = "/{characterId}/thumbnail", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Upload thumbnail image file for character")
    public CharacterDto uploadImage(@PathVariable("characterId") int characterId, 
                                    @RequestParam("file") MultipartFile file) {
        return characterService.uploadCharacterThumbnail(characterId, file);
    }
    
    @GetMapping(path = "/{characterId}/thumbnail")
    @Operation(summary = "Get character thumbnail image file")
    public ResponseEntity<Resource> getCharacterThumbnail(@PathVariable("characterId") int characterId) {
        Resource file = characterService.getCharacterThumbnail(characterId);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .contentType(MediaType.parseMediaType("image/all"))
                .body(file);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Save character")
    public CharacterDto saveCharacter(@Valid @RequestBody CharacterDto character) {
        return characterService.saveCharacter(character);
    }
}
