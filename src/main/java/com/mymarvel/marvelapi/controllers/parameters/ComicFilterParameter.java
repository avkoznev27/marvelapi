package com.mymarvel.marvelapi.controllers.parameters;

import java.util.List;

import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.Parameter;

public class ComicFilterParameter {
    
    @Parameter(description = "Return comics within a predefined date range. Dates must be specified as date1,date2 (e.g. 2013-01-01,2013-01-02). Dates are formatted as YYYY-MM-DD")
    @Size(min = 2, message = "There must be two date range")
    private List<String> dateRange;
    @Parameter(description = "Return only issues in series whose title matches the input.")
    private String title;
    @Parameter(description = "Return only issues in series whose title starts with the input.")
    private String titleStartsWith;
    @Parameter(description = "Return only issues in series whose start year matches the input.")
    private String startYear;
    @Parameter(description = "Return only comics which have been modified since the specified date. Date is formatted as YYYY-MM-DD")
    private String modifiedSince;
    
    public ComicFilterParameter(@Size(min = 2, message = "There must be two date range") List<String> dateRange,
            String title, String titleStartsWith, String startYear, String modifiedSince) {
        this.dateRange = dateRange;
        this.title = title;
        this.titleStartsWith = titleStartsWith;
        this.startYear = startYear;
        this.modifiedSince = modifiedSince;
    }
    
    public ComicFilterParameter() {
    }

    public List<String> getDateRange() {
        return dateRange;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getTitleStartsWith() {
        return titleStartsWith;
    }
    
    public String getStartYear() {
        return startYear;
    }
    
    public String getModifiedSince() {
        return modifiedSince;
    }
    
    public void setDateRange(List<String> dateRange) {
        this.dateRange = dateRange;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setTitleStartsWith(String titleStartsWith) {
        this.titleStartsWith = titleStartsWith;
    }
    
    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }
    
    public void setModifiedSince(String modifiedSince) {
        this.modifiedSince = modifiedSince;
    }

    @Override
    public String toString() {
        return String.format("dateRange=%s, title=%s, titleStartsWith=%s, startYear=%s, modifiedSince=%s", 
                dateRange, title, titleStartsWith, startYear, modifiedSince);
    }
}
