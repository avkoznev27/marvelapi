package com.mymarvel.marvelapi.controllers.parameters;

import io.swagger.v3.oas.annotations.Parameter;

public class CharacterFilterParameter {
    
    @Parameter(description = "Return only characters matching the specified full character name (e.g. Spider-Man).") 
    private String name;
    @Parameter(description = "Return characters with names that begin with the specified string (e.g. Sp).") 
    private String nameStartsWith;
    @Parameter(description = "Return only characters which have been modified since the specified date. Date is formatted as YYYY-MM-DD")
    private String modifiedSince;
    
    public CharacterFilterParameter(String name, String nameStartsWith, String modifiedSince) {
        this.name = name;
        this.nameStartsWith = nameStartsWith;
        this.modifiedSince = modifiedSince;
    }
    
    public CharacterFilterParameter() {
    }
    
    public String getName() {
        return name;
    }
    public String getNameStartsWith() {
        return nameStartsWith;
    }
    public String getModifiedSince() {
        return modifiedSince;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setNameStartsWith(String nameStartsWith) {
        this.nameStartsWith = nameStartsWith;
    }
    public void setModifiedSince(String modifiedSince) {
        this.modifiedSince = modifiedSince;
    }
    
    @Override
    public String toString() {
        return String.format("name=%s, nameStartsWith=%s, modifiedSince=%s", name, nameStartsWith, modifiedSince);
    }  
}
