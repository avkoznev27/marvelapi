package com.mymarvel.marvelapi.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springdoc.api.annotations.ParameterObject;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.service.ComicService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("${api.comics.base.path}")
@Tag(name = "Comics")
public class ComicsController {
    
    private ComicService comicService;
    
    public ComicsController(ComicService comicService) {
        this.comicService = comicService;
    }

    @GetMapping
    @Operation(summary = "Fetches lists of comics.")
    public Page<ComicDto> getComics(@ParameterObject @Valid ComicFilterParameter parameters,
                                        @Parameter(description = "Return only comics which feature the specified characters (accepts a comma-separated list of ids).")
                                        @RequestParam(required = false, name = "characters") List<Integer> characterIds, 
                                        @ParameterObject Pageable pageable) {
        return comicService.findAllComics(parameters, characterIds, pageable);
    }
    
    @GetMapping("/{comicId}")
    @Operation(summary = "Fetches a single comic by id.")
    public ComicDto getComicById(@PathVariable("comicId") int comicId) {
        return comicService.findComicById(comicId);
    }
    
    @GetMapping("/{comicId}/characters")
    @Operation(summary = "Fetches lists of characters filtered by a comic id.")
    public Page<CharacterDto> getCharactersByComicId(@PathVariable("comicId") int comicId,
                                                     @ParameterObject CharacterFilterParameter parameters, 
                                                     @ParameterObject Pageable pageable) {
        return comicService.findCharactersByComicId(comicId, parameters, pageable);
    }
    
    @PostMapping(value = "/{comicId}/thumbnail", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Upload thumbnail image file for comic")
    public ComicDto uploadImage(@PathVariable("comicId") int comicId, 
                                @RequestParam("file") MultipartFile file) {
        return comicService.uploadComicThumbnail(comicId, file);
    }
    
    @GetMapping(path = "/{comicId}/thumbnail")
    @Operation(summary = "Get comic thumbnail image file")
    public ResponseEntity<Resource> getComicThumbnail(@PathVariable("comicId") int comicId) {
        Resource file = comicService.getComicThumbnail(comicId);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .contentType(MediaType.parseMediaType("image/all"))
                .body(file);
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Save comic")
    public ComicDto saveComic(@Valid @RequestBody ComicDto comic) {
        return comicService.saveComic(comic);
    }
}
