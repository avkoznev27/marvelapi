package com.mymarvel.marvelapi.controllers;

import java.time.format.DateTimeParseException;

import javax.persistence.EntityNotFoundException;

import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.mymarvel.marvelapi.exceptions.MarvelApiError;
import com.mymarvel.marvelapi.exceptions.ResourceNotFoundException;
import com.mymarvel.marvelapi.exceptions.UploadFileException;

@RestControllerAdvice
public class MarvelExceptionHandler {
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    public MarvelApiError handleResourceNotFoundException(ResourceNotFoundException ex) {
        return new MarvelApiError(HttpStatus.NOT_FOUND, ex);
    }
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public MarvelApiError handleEntityNotFoundException(EntityNotFoundException ex) {
        return new MarvelApiError(HttpStatus.NOT_FOUND, ex);
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DateTimeParseException.class)
    public MarvelApiError handleDateTimeParseException(DateTimeParseException ex) {
        return new MarvelApiError(HttpStatus.BAD_REQUEST, ex);
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(PropertyReferenceException.class)
    public MarvelApiError handlePropertyReferenceException(PropertyReferenceException ex) {
        return new MarvelApiError(HttpStatus.BAD_REQUEST, ex);
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UploadFileException.class)
    public MarvelApiError handleUploadFileException(UploadFileException ex) {
        return new MarvelApiError(HttpStatus.BAD_REQUEST, ex);
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public MarvelApiError handleValidationExceptions(BindException ex) {
        MarvelApiError marvelApiError = new MarvelApiError(HttpStatus.BAD_REQUEST, ex);
        marvelApiError.addFieldValidationErrors(ex.getFieldErrors());
        marvelApiError.addGlobalValidationErrors(ex.getGlobalErrors());
        return marvelApiError;
    }
}
