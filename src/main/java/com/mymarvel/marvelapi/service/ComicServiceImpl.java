package com.mymarvel.marvelapi.service;

import static org.springframework.util.Assert.notNull;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.exceptions.ResourceNotFoundException;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.repository.CharacterRepository;
import com.mymarvel.marvelapi.repository.ComicRepository;
import com.mymarvel.marvelapi.service.specifications.CharacterSpecification;
import com.mymarvel.marvelapi.service.specifications.ComicSpecification;
import com.mymarvel.marvelapi.utils.FilesUtil;
import com.mymarvel.marvelapi.utils.mapper.CharacterMapper;
import com.mymarvel.marvelapi.utils.mapper.ComicMapper;

@Service
@Transactional
public class ComicServiceImpl implements ComicService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ComicServiceImpl.class);

    private ComicRepository comicRepository;
    private CharacterRepository characterRepository;
    private CharacterMapper characterMapper;
    private ComicMapper comicMapper;
    private FilesUtil filesUtil;
    
    public ComicServiceImpl(ComicRepository comicRepository, 
                            CharacterRepository characterRepository, 
                            ComicMapper comicMapper, 
                            CharacterMapper characterMapper,
                            FilesUtil filesUtil) {
        this.comicRepository = comicRepository;
        this.characterRepository = characterRepository;
        this.comicMapper = comicMapper;
        this.characterMapper = characterMapper;
        this.filesUtil = filesUtil;
    }

    @Override
    public Page<ComicDto> findAllComics(ComicFilterParameter filterParameter, List<Integer> characterIds, Pageable pageable) {
        LOGGER.debug("Getting comics with filter parameters : {}, charactersIds = {}", filterParameter, characterIds);
        notNull(filterParameter, "Filter parameters must not be null");
        return comicRepository.findAll(new ComicSpecification(characterIds, filterParameter), pageable)
                .map(comic -> comicMapper.toDto(comic));
    }
    
    @Override
    public ComicDto findComicById(int comicId) {
        LOGGER.debug("Getting comic by id = {}", comicId);
        return comicMapper.toDto(comicRepository.findById(comicId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("Comic with id = %d doesn't exists", comicId))));
    }

    @Override
    public Page<CharacterDto> findCharactersByComicId(int comicId, CharacterFilterParameter filterParameter, Pageable pageable) {
        LOGGER.debug("Getting charcters by comic id = {} with filter parameters : {}", comicId, filterParameter);
        notNull(filterParameter, "Filter parameters must not be null");
        return characterRepository.findAll(new CharacterSpecification(comicId, filterParameter), pageable)
                .map(characterMapper::toDto);
    }

    @Override
    public ComicDto saveComic(ComicDto comicDto) {
        LOGGER.debug("Saving comic {}", comicDto);
        notNull(comicDto, "Comic must not be null");
        comicDto.setModified(LocalDate.now());
        return comicMapper.toDto(comicRepository.save(comicMapper.toEntity(comicDto)));
    }
    
    @Override
    public ComicDto uploadComicThumbnail (int comicId, MultipartFile imageFile) {
        LOGGER.debug("Uploading comic thumbnail {}", imageFile);
        notNull(imageFile, "File must not be null");
        ComicDto comicDto = findComicById(comicId);
        String fileName = filesUtil.uploadFile(imageFile);
        comicDto.setThumbnailName(fileName);
        return saveComic(comicDto);
    }
    
    @Override
    public Resource getComicThumbnail(int comicId) {
        LOGGER.debug("Getting thumbnail file for comic with id = {}", comicId);
        ComicDto comicDto = findComicById(comicId);
        String thumbnailName = comicDto.getThumbnailName();
        if (thumbnailName != null) {
            return filesUtil.loadFileAsResource(thumbnailName);
        } else {
            throw new ResourceNotFoundException(
                    String.format("The thumbnail for the comic with id = %d has not been loaded yet", comicId)) ;
        }   
    }
}
