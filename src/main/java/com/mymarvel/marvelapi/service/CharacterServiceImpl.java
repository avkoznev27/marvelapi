package com.mymarvel.marvelapi.service;

import static org.springframework.util.Assert.notNull;

import java.time.LocalDate;
import java.util.List;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.exceptions.ResourceNotFoundException;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.repository.CharacterRepository;
import com.mymarvel.marvelapi.repository.ComicRepository;
import com.mymarvel.marvelapi.service.specifications.CharacterSpecification;
import com.mymarvel.marvelapi.service.specifications.ComicSpecification;
import com.mymarvel.marvelapi.utils.FilesUtil;
import com.mymarvel.marvelapi.utils.mapper.CharacterMapper;
import com.mymarvel.marvelapi.utils.mapper.ComicMapper;

@Service
@Transactional
public class CharacterServiceImpl implements CharacterService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CharacterServiceImpl.class);
    
    private CharacterRepository characterRepository;
    private ComicRepository comicRepository;
    private CharacterMapper characterMapper;
    private ComicMapper comicMapper;
    private FilesUtil filesUtil;
    
    @Autowired
    public CharacterServiceImpl(CharacterRepository characterRepository, 
                                ComicRepository comicRepository, 
                                CharacterMapper characterMapper, 
                                ComicMapper comicMapper,
                                FilesUtil filesUtil) {
        this.characterRepository = characterRepository;
        this.comicRepository = comicRepository;
        this.characterMapper = characterMapper;
        this.comicMapper = comicMapper;
        this.filesUtil = filesUtil;
    }

    @Override
    public Page<CharacterDto> findAllCharacters(CharacterFilterParameter filterParameter, List<Integer> comicsIds, Pageable pageable) {
        LOGGER.debug("Getting characters with filter parameters : {}, comicsIds = {}", filterParameter, comicsIds);
        notNull(filterParameter, "Filter parameters must not be null");
        return characterRepository.findAll(new CharacterSpecification(comicsIds, filterParameter), pageable)
                .map(characterMapper::toDto);
    }

    @Override
    public CharacterDto findCharacterById(int characterId) {
        LOGGER.debug("Getting character by id = {}", characterId);
        return characterMapper.toDto(characterRepository.findById(characterId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("Character with id = %d doesn't exists", characterId))));
    }

    @Override
    public Page<ComicDto> findComicsByCharacterId(int characterId, ComicFilterParameter filterParameter, Pageable pageable) {
        LOGGER.debug("Getting comics by character id = {} with filter parameters : {}", characterId, filterParameter);
        notNull(filterParameter, "Filter parameters must not be null");
        return comicRepository.findAll(new ComicSpecification(characterId, filterParameter), pageable)
                .map(comicMapper::toDto);
    }
    
    @Override
    public CharacterDto saveCharacter(CharacterDto characterDto) {
        LOGGER.debug("Saving character {}", characterDto);
        notNull(characterDto, "Character must not be null");
        characterDto.setModified(LocalDate.now());
        return characterMapper.toDto(characterRepository.save(characterMapper.toEntity(characterDto)));
    }

    @Override
    public CharacterDto uploadCharacterThumbnail (int characterId, MultipartFile imageFile) {
        LOGGER.debug("Uploading character thumbnail {}", imageFile);
        notNull(imageFile, "File must not be null");
        CharacterDto characterDto = findCharacterById(characterId);
        String fileName = filesUtil.uploadFile(imageFile);
        characterDto.setThumbnailName(fileName);
        return saveCharacter(characterDto);
    }
    
    @Override
    public Resource getCharacterThumbnail(int characterId) {
        LOGGER.debug("Getting thumbnail file for character with id = {}", characterId);
        CharacterDto characterDto = findCharacterById(characterId);
        String thumbnailName = characterDto.getThumbnailName();
        if (thumbnailName != null) {
            return filesUtil.loadFileAsResource(thumbnailName);
        } else {
            throw new ResourceNotFoundException(
                    String.format("The thumbnail for the character with id = %d has not been loaded yet", characterId)) ;
        }   
    }
}
