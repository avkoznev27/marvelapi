package com.mymarvel.marvelapi.service;

import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;

public interface ComicService {
    
    Page<ComicDto> findAllComics(ComicFilterParameter parameters, List<Integer> characterIds, Pageable pageable);

    ComicDto findComicById(int comicId);
    
    Page<CharacterDto> findCharactersByComicId(int comicId, CharacterFilterParameter parameters, Pageable pageable);

    ComicDto saveComic(ComicDto comic);
    
    ComicDto uploadComicThumbnail(int comicId, MultipartFile imageFile);

    Resource getComicThumbnail(int comicId);
      
}
