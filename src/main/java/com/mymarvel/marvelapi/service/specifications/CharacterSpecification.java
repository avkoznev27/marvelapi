package com.mymarvel.marvelapi.service.specifications;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;

public class CharacterSpecification implements Specification<Character> {

    private static final long serialVersionUID = 4264403888315614535L;

    private CharacterFilterParameter parameters;
    private List<Integer> comicsIds;
    private int comicId;

    public CharacterSpecification(CharacterFilterParameter parameters) {
        this.parameters = parameters;
    }
    
    public CharacterSpecification(int comicId, CharacterFilterParameter parameters) {
        this.comicId = comicId;
        this.parameters = parameters;
    }
    
    public CharacterSpecification(List<Integer> comicsIds, CharacterFilterParameter parameters) {
        this.comicsIds = comicsIds;
        this.parameters = parameters;
    }

    @Override
    public Predicate toPredicate(Root<Character> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        String name = parameters.getName();
        if (name != null && !name.isEmpty()) {
            predicates.add(criteriaBuilder.equal(root.get("name"), name));
        }
        
        String nameStartsWith = parameters.getNameStartsWith();
        if (nameStartsWith != null && !nameStartsWith.isEmpty()) {
            predicates.add(criteriaBuilder.like(root.get("name"), nameStartsWith + "%"));
        }
        
        String modifiedSince = parameters.getModifiedSince();
        if (modifiedSince != null && !modifiedSince.isEmpty()) {
            LocalDate modified = LocalDate.parse(modifiedSince);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("modified"), modified));
        }
        
        if (comicId > 0) {
            query.distinct(true);
            Root<Character> character = root;
            Root<Comic> comic = query.from(Comic.class);
            Expression<Collection<Character>> comicCharacters = comic.get("characters");
            predicates.add(criteriaBuilder.and(criteriaBuilder.equal(comic.get("comicId"), comicId), 
                                               criteriaBuilder.isMember(character, comicCharacters)));
        }
        
        if (comicsIds != null && !comicsIds.isEmpty()) {
            query.distinct(true);
            Root<Character> character = root;
            Subquery<Comic> comicSubQuery = query.subquery(Comic.class);
            Root<Comic> comic = comicSubQuery.from(Comic.class);
            Expression<Collection<Character>> comicCharacters = comic.get("characters");
            comicSubQuery.select(comic);
            comicSubQuery.where(criteriaBuilder.in(comic.get("comicId")).value(comicsIds), 
                                criteriaBuilder.isMember(character, comicCharacters));
            predicates.add(criteriaBuilder.exists(comicSubQuery));
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
