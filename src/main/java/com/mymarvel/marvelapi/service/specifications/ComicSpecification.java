package com.mymarvel.marvelapi.service.specifications;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;

public class ComicSpecification implements Specification<Comic> {
    
    private static final long serialVersionUID = 5520032107627254521L;
    
    private ComicFilterParameter filterParameter;
    private List<Integer> characterIds;
    private int characterId;
    
    public ComicSpecification(ComicFilterParameter filterParameter) {
        this.filterParameter = filterParameter;
    }
    
    public ComicSpecification(List<Integer> characterIds, ComicFilterParameter filterParameter) {
        this.characterIds = characterIds;
        this.filterParameter = filterParameter;
    }
    
    public ComicSpecification(int characterId, ComicFilterParameter filterParameter) {
        this.characterId = characterId;
        this.filterParameter = filterParameter;
    }

    @Override
    public Predicate toPredicate(Root<Comic> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        
        List<String> dateRange = filterParameter.getDateRange();
        if (dateRange != null && !dateRange.isEmpty()) {
            LocalDate startPeriod = LocalDate.parse(dateRange.get(0));
            LocalDate endPeriod = LocalDate.parse(dateRange.get(1));
            predicates.add(criteriaBuilder.between(root.get("startDate"), startPeriod, endPeriod));
        }
        
        String title = filterParameter.getTitle();
        if (title != null && !title.isEmpty()) {
            predicates.add(criteriaBuilder.equal(root.get("title"), title));
        }
        
        String titleStartsWith = filterParameter.getTitleStartsWith();
        if (titleStartsWith != null && !titleStartsWith.isEmpty()) {
            predicates.add(criteriaBuilder.like(root.get("title"), titleStartsWith + "%"));
        }
        
        String startYear = filterParameter.getStartYear();
        if (startYear != null && !startYear.isEmpty()) {
            LocalDate yearDate = Year.parse(startYear).atDay(1);
            predicates.add(criteriaBuilder.between(root.get("startDate"), 
                    yearDate.with(firstDayOfYear()), yearDate.with(lastDayOfYear())));
        }
        
        String modifiedSince = filterParameter.getModifiedSince();
        if (modifiedSince != null && !modifiedSince.isEmpty()) {
            LocalDate modified = LocalDate.parse(modifiedSince);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("modified"), modified));
        }
        
        if (characterId > 0) {
            query.distinct(true);
            Root<Comic> comic = root;
            Root<Character> character = query.from(Character.class);
            Expression<Collection<Comic>> characterComics = character.get("comics");
            predicates.add(criteriaBuilder.and(criteriaBuilder.equal(character.get("characterId"), characterId), 
                                               criteriaBuilder.isMember(comic, characterComics)));
        }
        
        if (characterIds != null && !characterIds.isEmpty()) {
            query.distinct(true);
            Root<Comic> comic = root;
            Subquery<Character> characterSubQuery = query.subquery(Character.class);
            Root<Character> character = characterSubQuery.from(Character.class);
            Expression<Collection<Comic>> characterComics = character.get("comics");
            characterSubQuery.select(character);
            characterSubQuery.where(criteriaBuilder.in(character.get("characterId")).value(characterIds), 
                                    criteriaBuilder.isMember(comic, characterComics));
            predicates.add(criteriaBuilder.exists(characterSubQuery)); 
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
