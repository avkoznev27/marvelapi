package com.mymarvel.marvelapi.service;

import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;

public interface CharacterService {
    
    Page<CharacterDto> findAllCharacters(CharacterFilterParameter parameters, List<Integer> comicsIds, Pageable pageable);

    CharacterDto findCharacterById(int characterId);

    Page<ComicDto> findComicsByCharacterId(int characterId, ComicFilterParameter comicsParameters, Pageable pageable);

    CharacterDto saveCharacter(CharacterDto character);
    
    CharacterDto uploadCharacterThumbnail(int characterId, MultipartFile imageFile);

    Resource getCharacterThumbnail(int characterId);
 
}
