package com.mymarvel.marvelapi.system;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;
import com.mymarvel.marvelapi.repository.CharacterRepository;
import com.mymarvel.marvelapi.utils.RestResponsePage;
import com.mymarvel.marvelapi.utils.mapper.CharacterMapper;
import com.mymarvel.marvelapi.utils.mapper.ComicMapper;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Sql(scripts = { "/test_tables.sql", "/testdata.sql" })
@AutoConfigureMockMvc
class CharactersSystemTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CharacterMapper characterMapper;
    @Autowired
    private ComicMapper comicMapper;
    @Autowired
    private CharacterRepository characterRepository;
    
   @Test
    void testGetCharactersShouldReturnAllCharactersFromDatabaseIfFilterParametersNotSet() throws Exception {
       String result = mockMvc.perform(get("/v1/public/characters")
               .param("size", "40"))
               .andDo(print())
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.content", hasSize(40)))
               .andReturn().getResponse().getContentAsString();
       List<Character> expectedCharacters = characterRepository.findAll();
       RestResponsePage<CharacterDto> responsePage = getCharacterResponsePage(result);
       assertEquals(expectedCharacters.size(), responsePage.getTotalElements());
   }
   
   @Test
   void testGetCharactersShouldReturnOnlyCharactersWithReqestedNameFromDataBase() throws Exception {
       CharacterFilterParameter filter = new CharacterFilterParameter("name1", null, null);
       String result = mockMvc.perform(get("/v1/public/characters")
               .param("name", filter.getName()))
               .andDo(print())
               .andExpect(status().isOk())
               .andReturn().getResponse().getContentAsString();
       List<Character> expectedCharacters = characterRepository.findAll().stream()
               .filter(getPredicateByName(filter.getName()))
               .collect(Collectors.toList());
       RestResponsePage<CharacterDto> responsePage = getCharacterResponsePage(result);
       assertEquals(expectedCharacters.size(), responsePage.getTotalElements());
       assertArrayEquals(expectedCharacters.stream().map(Character::getCharacterId).toArray(), 
               responsePage.getContent().stream().map(CharacterDto::getId).toArray());
   }
   
   @Test
   void testGetCharactersShouldReturnOnlyCharactersWithReqestedNameStartsWithParameterFromDataBase() throws Exception {
       CharacterFilterParameter filter = new CharacterFilterParameter(null, "name1", null);
       String result = mockMvc.perform(get("/v1/public/characters")
               .param("size", "40")
               .param("nameStartsWith", filter.getNameStartsWith()))
               .andDo(print())
               .andExpect(status().isOk())
               .andReturn().getResponse().getContentAsString();
       List<Character> expectedCharacters = characterRepository.findAll().stream()
               .filter(getPredicateByNameStartsWith(filter.getNameStartsWith()))
               .collect(Collectors.toList());
       RestResponsePage<CharacterDto> responsePage = getCharacterResponsePage(result);
       assertEquals(expectedCharacters.size(), responsePage.getTotalElements());
       assertArrayEquals(expectedCharacters.stream().map(Character::getCharacterId).toArray(), 
               responsePage.getContent().stream().map(CharacterDto::getId).toArray());
   }
   
   @Test
   void testGetCharactersShouldReturnOnlyCharactersModifiedAfterRequestedDate() throws Exception {
       CharacterFilterParameter filter = new CharacterFilterParameter(null, null, "2021-04-01");
       String result = mockMvc.perform(get("/v1/public/characters")
               .param("size", "40")
               .param("modifiedSince", filter.getModifiedSince()))
               .andDo(print())
               .andExpect(status().isOk())
               .andReturn().getResponse().getContentAsString();
       List<Character> expectedCharacters = characterRepository.findAll().stream()
               .filter(getPredicateByModifiedDate(filter.getModifiedSince()))
               .collect(Collectors.toList());
       RestResponsePage<CharacterDto> responsePage = getCharacterResponsePage(result);
       assertEquals(expectedCharacters.size(), responsePage.getTotalElements());
       assertArrayEquals(expectedCharacters.stream().map(Character::getCharacterId).toArray(), 
               responsePage.getContent().stream().map(CharacterDto::getId).toArray());
   }
   
   @Test
   void testGetCharactersShouldReturnBadRequestWhenRequestedDateHasWrongFormat() throws Exception {
       String modifiedDate = "2021-99-01";
       mockMvc.perform(get("/v1/public/characters")
               .param("size", "40")
               .param("modifiedSince", modifiedDate))
               .andDo(print())
               .andExpect(status().isBadRequest());
   }
   
   @Test
   void testGetCharactersShouldReturnOnlyCharactersWithAllFilterParameters() throws Exception {
       CharacterFilterParameter filter = new CharacterFilterParameter("name1", "n", "2021-03-10");
       String result = mockMvc.perform(get("/v1/public/characters")
               .param("size", "40")
               .param("name", filter.getName())
               .param("nameStartsWith", filter.getNameStartsWith())
               .param("modifiedSince", filter.getModifiedSince()))
               .andDo(print())
               .andExpect(status().isOk())
               .andReturn().getResponse().getContentAsString();
       List<Character> expectedCharacters = characterRepository.findAll().stream()
               .filter(getPredicateByName(filter.getName()))
               .filter(getPredicateByNameStartsWith(filter.getNameStartsWith()))
               .filter(getPredicateByModifiedDate(filter.getModifiedSince()))
               .collect(Collectors.toList());
       RestResponsePage<CharacterDto> responsePage = getCharacterResponsePage(result);
       assertEquals(expectedCharacters.size(), responsePage.getTotalElements());
       assertArrayEquals(expectedCharacters.stream().map(Character::getCharacterId).toArray(), 
               responsePage.getContent().stream().map(CharacterDto::getId).toArray());
   }
   
   @Test
   void testGetCharactersShouldReturnOnlyCharactersWithNameStarstWithAndHavingComicsAsRequestedComicList() throws Exception {
       CharacterFilterParameter filter = new CharacterFilterParameter(null, "name1", null);
       String filterByComicsIds = "1,2,3";
       String result = mockMvc.perform(get("/v1/public/characters")
               .param("size", "40")
               .param("nameStartsWith", filter.getNameStartsWith())
               .param("comics", filterByComicsIds))
               .andDo(print())
               .andExpect(status().isOk())
               .andReturn().getResponse().getContentAsString();
       
       List<Integer> comicIds = Arrays.asList(filterByComicsIds.split(",")).stream()
               .mapToInt(Integer::parseInt)
               .boxed()
               .collect(Collectors.toList());
       
       List<Character> expectedCharacters = characterRepository.findAll().stream()
               .filter(getPredicateByNameStartsWith(filter.getNameStartsWith()))
               .filter(getPredicateByComicIds(comicIds))
               .collect(Collectors.toList());
       
       RestResponsePage<CharacterDto> responsePage = getCharacterResponsePage(result);
       assertEquals(expectedCharacters.size(), responsePage.getTotalElements());
       assertArrayEquals(expectedCharacters.stream().map(Character::getCharacterId).toArray(), 
               responsePage.getContent().stream().map(CharacterDto::getId).toArray());
   } 
   
   @Test
   void testGetCharacterByIdShouldReturnFoundCharacterWhenExistingCharacterIdPassed() throws Exception {
       int characterId = 1;
       String result = mockMvc.perform(get("/v1/public/characters/{characterId}", characterId))
               .andDo(print())
               .andExpect(status().isOk())
               .andReturn().getResponse().getContentAsString();
       Character expected = characterRepository.findById(characterId).get();
       CharacterDto actualDto = objectMapper.readValue(result, CharacterDto.class);
       Character actual = characterMapper.toEntity(actualDto);
       assertEquals(expected, actual);
   }
   
   @Test
   void testGetComicsByCharacterIdShouldReturnAllComicsByPassedCharacterId() throws Exception {
       int characterId = 1;
       String result = mockMvc.perform(get("/v1/public/characters/{characterId}/comics", characterId)
               .param("size", "40"))
               .andDo(print())
               .andExpect(status().isOk())
               .andReturn().getResponse().getContentAsString();
       Set<Comic> expected = characterRepository.findById(characterId).get().getComics();
       expected.forEach(System.out::println);
       HashSet<Comic> actual = new HashSet<>(getComicResponsePage(result).map(comicMapper::toEntity).getContent());
       assertEquals(expected, actual);
   }
   
   @Test
   void testSaveCharacterShouldSavePassedCharacterToDatabase() throws Exception {
       CharacterDto testCharacter = new CharacterDto();
       testCharacter.setName("new character");
       long countBeforeSaving = characterRepository.count();
       mockMvc.perform(post("/v1/public/characters")
               .contentType(MediaType.APPLICATION_JSON)
               .content(objectMapper.writeValueAsString(testCharacter)))
       .andDo(print())
       .andExpect(status().isCreated());
       long countAftereSaving = characterRepository.count();
       assertEquals(1, countAftereSaving - countBeforeSaving);
   }
   
   @Test
   void testSaveCharacterShouldUpdateCharacterWhenPassedCharacterExistsInDatabase() throws Exception {
       int characterId = 1;
       String updatedName = "new character";
       CharacterDto testCharacter = new CharacterDto();
       testCharacter.setId(characterId);
       testCharacter.setName(updatedName);
       Character characterBeforeUpdate = characterRepository.findById(characterId).get();
       mockMvc.perform(post("/v1/public/characters")
               .contentType(MediaType.APPLICATION_JSON)
               .content(objectMapper.writeValueAsString(testCharacter)))
       .andDo(print())
       .andExpect(status().isCreated());
       Character characterAfterUpdate = characterRepository.findById(characterId).get();
       assertNotEquals(characterBeforeUpdate.getName(), characterAfterUpdate.getName());
       assertEquals(updatedName, characterAfterUpdate.getName());
   }
   
   private Predicate<Character> getPredicateByName(String name) {
       return character -> character.getName().equals(name);
   }
   
   private Predicate<Character> getPredicateByNameStartsWith(String nameStartsWith) {
       return character -> character.getName().startsWith(nameStartsWith);
   }
   
   private Predicate<Character> getPredicateByModifiedDate(String modifiedDate) {
       return character -> character.getModified().isAfter(LocalDate.parse(modifiedDate))
               || character.getModified().equals(LocalDate.parse(modifiedDate));
   }
   
   private Predicate<Character> getPredicateByComicIds(List<Integer> comicIds) {
       return character -> character.getComics().stream()
               .anyMatch(comic -> comicIds.contains(comic.getComicId())); 
   }
   
   private RestResponsePage<CharacterDto> getCharacterResponsePage(String result) throws Exception {
       return objectMapper.readValue(result, new TypeReference<RestResponsePage<CharacterDto>>() {}); 
   }

   private RestResponsePage<ComicDto> getComicResponsePage(String result) throws Exception {
       return objectMapper.readValue(result, new TypeReference<RestResponsePage<ComicDto>>() {}); 
   }
}
