package com.mymarvel.marvelapi.system;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;
import com.mymarvel.marvelapi.repository.ComicRepository;
import com.mymarvel.marvelapi.utils.RestResponsePage;
import com.mymarvel.marvelapi.utils.mapper.CharacterMapper;
import com.mymarvel.marvelapi.utils.mapper.ComicMapper;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Sql(scripts = { "/test_tables.sql", "/testdata.sql" })
@AutoConfigureMockMvc
class ComicsSystemTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CharacterMapper characterMapper;
    @Autowired
    private ComicMapper comicMapper;
    @Autowired
    private ComicRepository comicRepository;
    
    @Test
    void testGetComicsShouldReturnAllComicsFromDatabaseIfFilterParametersNotSet() throws Exception {
       String result = mockMvc.perform(get("/v1/public/comics")
               .param("size", "40"))
               .andDo(print())
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.content", hasSize(40)))
               .andReturn().getResponse().getContentAsString();
       List<Comic> expectedComics = comicRepository.findAll();
       RestResponsePage<ComicDto> responsePage = getComicResponsePage(result);
       assertEquals(expectedComics.size(), responsePage.getTotalElements());
   }
    
    @Test
    void testGetComicsShouldReturnComicsWithStartDatePassedDatePeriod() throws Exception {
        String dateRangeString = "2000-01-01,2010-01-01";
        List<String> dateRange = Arrays.asList(dateRangeString.split(","));
        String result = mockMvc.perform(get("/v1/public/comics")
                .param("size", "40")
                .param("dateRange", dateRangeString))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        List<Comic> expectedComics = comicRepository.findAll().stream()
                .filter(getPredicateByDateRange(dateRange))
                .collect(Collectors.toList());
        RestResponsePage<ComicDto> responsePage = getComicResponsePage(result);
        assertEquals(expectedComics.size(), responsePage.getTotalElements());
        assertArrayEquals(expectedComics.stream().map(Comic::getComicId).toArray(), 
                responsePage.getContent().stream().map(ComicDto::getId).toArray());
    }
    
    @Test
    void testGetComicsShouldReturnBadRequestWhenPassedDatePeriodWithWrongDateOrContainsOnlyOneDate() throws Exception {
        mockMvc.perform(get("/v1/public/comics")
                .param("dateRange", "2020-01-01"))
                .andDo(print())
                .andExpect(status().isBadRequest());
        
        mockMvc.perform(get("/v1/public/comics")
                .param("dateRange", "2020-99-01, 2020-10-01"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
    
    @Test
    void testGetComicsShouldReturnOnlyComicsWithReqestedTitleFromDataBase() throws Exception {
        ComicFilterParameter filter = new ComicFilterParameter(null, "title1", null, null, null);
        String result = mockMvc.perform(get("/v1/public/comics")
                .param("title", filter.getTitle()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        List<Comic> expectedComics = comicRepository.findAll().stream()
                .filter(getPredicateByTitle(filter.getTitle()))
                .collect(Collectors.toList());
        RestResponsePage<ComicDto> responsePage = getComicResponsePage(result);
        assertEquals(expectedComics.size(), responsePage.getTotalElements());
        assertArrayEquals(expectedComics.stream().map(Comic::getComicId).toArray(), 
                responsePage.getContent().stream().map(ComicDto::getId).toArray());
    }
    
    @Test
    void testGetComicsShouldReturnOnlyComicsWithReqestedNameStartsWithParameterFromDataBase() throws Exception {
        ComicFilterParameter filter = new ComicFilterParameter(null, null, "title1", null, null);
        String result = mockMvc.perform(get("/v1/public/comics")
                .param("size", "40")
                .param("titleStartsWith", filter.getTitleStartsWith()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        List<Comic> expectedComics = comicRepository.findAll().stream()
                .filter(getPredicateByTitleStartsWith(filter.getTitleStartsWith()))
                .collect(Collectors.toList());
        RestResponsePage<ComicDto> responsePage = getComicResponsePage(result);
        assertEquals(expectedComics.size(), responsePage.getTotalElements());
        assertArrayEquals(expectedComics.stream().map(Comic::getComicId).toArray(), 
                responsePage.getContent().stream().map(ComicDto::getId).toArray());
    }
    
    @Test
    void testGetComicsShouldReturnOnlyComicsWithStartDateInRequestedYear() throws Exception {
        ComicFilterParameter filter = new ComicFilterParameter(null, null, null, "2000", null);
        String result = mockMvc.perform(get("/v1/public/comics")
                .param("size", "40")
                .param("startYear", filter.getStartYear()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        List<Comic> expectedComics = comicRepository.findAll().stream()
                .filter(getPredicateByStartYear(filter.getStartYear()))
                .collect(Collectors.toList());
        RestResponsePage<ComicDto> responsePage = getComicResponsePage(result);
        assertEquals(expectedComics.size(), responsePage.getTotalElements());
        assertArrayEquals(expectedComics.stream().map(Comic::getComicId).toArray(), 
                responsePage.getContent().stream().map(ComicDto::getId).toArray());
    }
    
    @Test
    void testGetComicsShouldReturnOnlyComicsModifiedAfterRequestedDate() throws Exception {
        ComicFilterParameter filter = new ComicFilterParameter(null, null, null, null, "2025-01-01");
        String result = mockMvc.perform(get("/v1/public/comics")
                .param("size", "40")
                .param("modifiedSince", filter.getModifiedSince()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        List<Comic> expectedComics = comicRepository.findAll().stream()
                .filter(getPredicateByModifiedDate(filter.getModifiedSince()))
                .collect(Collectors.toList());
        RestResponsePage<ComicDto> responsePage = getComicResponsePage(result);
        assertEquals(expectedComics.size(), responsePage.getTotalElements());
        assertArrayEquals(expectedComics.stream().map(Comic::getComicId).toArray(), 
                responsePage.getContent().stream().map(ComicDto::getId).toArray());
    }
    
    @Test
    void testGetComicsShouldReturnBadRequestWhenRequestedDateHasWrongFormat() throws Exception {
        String modifiedDate = "2021-99-01";
        mockMvc.perform(get("/v1/public/comics")
                .param("size", "40")
                .param("modifiedSince", modifiedDate))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
    
    @Test
    void testGetComicsShouldReturnOnlyComicsWithTitleStartsWithAndHavingCharactersFromPassedListCharacterIds() throws Exception {
        ComicFilterParameter filter = new ComicFilterParameter(null, null, "title1", null, null);
        String filterByCharacterIds = "1,2,3";
        String result = mockMvc.perform(get("/v1/public/comics")
                .param("size", "40")
                .param("titleStartsWith", filter.getTitleStartsWith())
                .param("characters", filterByCharacterIds))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        List<Integer> characterIds = Arrays.asList(filterByCharacterIds.split(",")).stream()
                .mapToInt(Integer::parseInt)
                .boxed()
                .collect(Collectors.toList());
        
        List<Comic> expectedComics = comicRepository.findAll().stream()
                .filter(getPredicateByTitleStartsWith(filter.getTitleStartsWith()))
                .filter(getPredicateByCharacterIds(characterIds))
                .collect(Collectors.toList());
        RestResponsePage<ComicDto> responsePage = getComicResponsePage(result);
        assertEquals(expectedComics.size(), responsePage.getTotalElements());
        assertArrayEquals(expectedComics.stream().map(Comic::getComicId).toArray(), 
                responsePage.getContent().stream().map(ComicDto::getId).toArray());
    }
    
    @Test
    void testGetComicsShouldReturnOnlyComicsWithPassedFilterParameters() throws Exception {
        ComicFilterParameter filter = new ComicFilterParameter(null, "title1", "t", "2000", null);
        String dateRangeString = "2000-01-01,2050-01-01";
        List<String> dateRange = Arrays.asList(dateRangeString.split(","));
        String result = mockMvc.perform(get("/v1/public/comics")
                .param("size", "40")
                .param("dateRange", dateRangeString)
                .param("title", filter.getTitle())
                .param("titleStartsWith", filter.getTitleStartsWith())
                .param("startYear", filter.getStartYear())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        List<Comic> expectedComics = comicRepository.findAll().stream()
                .filter(getPredicateByDateRange(dateRange))
                .filter(getPredicateByTitle(filter.getTitle()))
                .filter(getPredicateByTitleStartsWith(filter.getTitleStartsWith()))
                .filter(getPredicateByStartYear(filter.getStartYear()))
                .collect(Collectors.toList());
        RestResponsePage<ComicDto> responsePage = getComicResponsePage(result);
        assertEquals(expectedComics.size(), responsePage.getTotalElements());
        assertArrayEquals(expectedComics.stream().map(Comic::getComicId).toArray(), 
                responsePage.getContent().stream().map(ComicDto::getId).toArray());
    }
    
    @Test
    void testGetComicByIdShouldReturnFoundComicWhenExistingComicIdPassed() throws Exception {
        int comicId = 1;
        String result = mockMvc.perform(get("/v1/public/comics/{comicId}", comicId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        Comic expected = comicRepository.findById(comicId).get();
        ComicDto actualDto = objectMapper.readValue(result, ComicDto.class);
        Comic actual = comicMapper.toEntity(actualDto);
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetCharactersByComicIdShouldReturnAllCharactersByPassedComicId() throws Exception {
        int comicId = 1;
        String result = mockMvc.perform(get("/v1/public/comics/{comicId}/characters", comicId)
                .param("size", "40"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        Set<Character> expected = comicRepository.findById(comicId).get().getCharacters();
        expected.forEach(System.out::println);
        HashSet<Character> actual = new HashSet<>(getCharacterResponsePage(result).map(characterMapper::toEntity).getContent());
        assertEquals(expected, actual);
    }
    
    @Test
    void testSaveComicShouldSavePassedComicToDatabase() throws Exception {
        ComicDto testComic = new ComicDto();
        testComic.setTitle("new comic");
        long countBeforeSaving = comicRepository.count();
        mockMvc.perform(post("/v1/public/comics")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testComic)))
        .andDo(print())
        .andExpect(status().isCreated());
        long countAftereSaving = comicRepository.count();
        assertEquals(1, countAftereSaving - countBeforeSaving);
    }
    
    @Test
    void testSaveComicShouldSavePassedComicToDatabaseWithCharactersFromListCharacterIds() throws Exception {
        String result = mockMvc.perform(post("/v1/public/comics")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"title\":\"new comic with characters\",\"characterIds\":[1,2,3]}")
                )
        .andDo(print())
        .andExpect(status().isCreated())
        .andReturn().getResponse().getContentAsString();
        int returnedComicId = objectMapper.readValue(result, ComicDto.class).getId();
        assertEquals(3, comicRepository.findById(returnedComicId).get().getCharacters().size());
    }
    
    @Test
    void testSaveComicShouldUpdateComicWhenPassedComicExistsInDatabase() throws Exception {
        int comicId = 1;
        String updatedTitle = "new comic";
        ComicDto testComic = new ComicDto();
        testComic.setId(comicId);
        testComic.setTitle(updatedTitle);
        Comic comicBeforeUpdate = comicRepository.findById(comicId).get();
        mockMvc.perform(post("/v1/public/comics")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testComic)))
        .andDo(print())
        .andExpect(status().isCreated());
        Comic comicAfterUpdate = comicRepository.findById(comicId).get();
        assertNotEquals(comicBeforeUpdate.getTitle(), comicAfterUpdate.getTitle());
        assertEquals(updatedTitle, comicAfterUpdate.getTitle());
    }
    
    private Predicate<Comic> getPredicateByDateRange(List<String> dateRange) {
        LocalDate startPeriod = LocalDate.parse(dateRange.get(0));
        LocalDate endPeriod = LocalDate.parse(dateRange.get(1));
        return comic -> (comic.getStartDate().isAfter(startPeriod) 
                || comic.getStartDate().isEqual(startPeriod)) && (comic.getStartDate().isBefore(endPeriod) 
                || comic.getStartDate().isEqual(endPeriod));
    }
    
    private Predicate<Comic> getPredicateByTitle(String title) {
        return comic -> comic.getTitle().equals(title);
    }
    
    private Predicate<Comic> getPredicateByTitleStartsWith(String titleStartsWith) {
        return comic -> comic.getTitle().startsWith(titleStartsWith);
    }
    
    private Predicate<Comic> getPredicateByStartYear(String startYear) {
        return comic -> comic.getStartDate().getYear() == Integer.parseInt(startYear);
    }
    
    private Predicate<Comic> getPredicateByModifiedDate(String modifiedDate) {
        return comic -> comic.getModified().isAfter(LocalDate.parse(modifiedDate))
                || comic.getModified().equals(LocalDate.parse(modifiedDate));
    }
    
    private Predicate<Comic> getPredicateByCharacterIds(List<Integer> characterIds) {
        return comic -> comic.getCharacters().stream()
                .anyMatch(character -> characterIds.contains(character.getCharacterId())); 
    }
    
    private RestResponsePage<CharacterDto> getCharacterResponsePage(String result) throws Exception {
        return objectMapper.readValue(result, new TypeReference<RestResponsePage<CharacterDto>>() {}); 
    }

    private RestResponsePage<ComicDto> getComicResponsePage(String result) throws Exception {
        return objectMapper.readValue(result, new TypeReference<RestResponsePage<ComicDto>>() {}); 
    }
}
