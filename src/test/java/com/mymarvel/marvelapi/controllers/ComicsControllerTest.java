package com.mymarvel.marvelapi.controllers;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.exceptions.ResourceNotFoundException;
import com.mymarvel.marvelapi.exceptions.UploadFileException;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.CharacterList;
import com.mymarvel.marvelapi.model.dto.CharacterSummary;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.model.dto.ComicList;
import com.mymarvel.marvelapi.model.dto.ComicSummary;
import com.mymarvel.marvelapi.service.ComicService;
import com.mymarvel.marvelapi.utils.RestResponsePage;

@WebMvcTest(controllers = ComicsController.class)
class ComicsControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    
    @MockBean
    private ComicService comicService;
    
    private CharacterDto testCharacter;
    private ComicDto testComic;

    @BeforeEach
    void setUp() throws Exception {
        testCharacter = createTestCharacterDto();
        testComic = createTestComicDto();
    }

    @Test
    void testGetComicsShouldReturnAllComics() throws Exception {
        when(comicService.findAllComics(any(ComicFilterParameter.class), any(), any(PageRequest.class)))
        .thenReturn(new PageImpl<>(singletonList(testComic)));
        String result = mockMvc.perform(get("/v1/public/comics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        List<ComicDto> pageContent = getComicPageContent(result);
        assertEquals(testComic, pageContent.get(0));
    }

    @Test
    void testGetComicByIdShouldReturnFoundComicWhenExistingComicIdPassed() throws Exception {
        when(comicService.findComicById(anyInt())).thenReturn(testComic);
        String result = mockMvc.perform(get("/v1/public/comics/{comicId}", 10))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(testComic.getId())))
        .andExpect(jsonPath("$.title", is(testComic.getTitle())))
        .andExpect(jsonPath("$.description", is(testComic.getDescription())))
        .andExpect(jsonPath("$.modified", is(testComic.getModified().toString())))
        .andExpect(jsonPath("$.startDate", is(testComic.getStartDate().toString())))
        .andExpect(jsonPath("$.price", is(testComic.getPrice())))
        .andExpect(jsonPath("$.pageCount", is(testComic.getPageCount())))
        .andExpect(jsonPath("$.resourceURI", is(testComic.getResourceURI())))
        .andExpect(jsonPath("$.thumbnailName", is(testComic.getThumbnailName())))
        .andExpect(jsonPath("$.characterList.available", is(testComic.getCharacterList().getAvailable())))
        .andExpect(jsonPath("$.characterList.collectionURI", is(testComic.getCharacterList().getCollectionURI())))
        .andExpect(jsonPath("$.characterList.items", hasSize(1)))
        .andExpect(jsonPath("$.characterList.items[0].resourceURI", is(testComic.getCharacterList().getItems().get(0).getResourceURI())))
        .andExpect(jsonPath("$.characterList.items[0].name", is(testComic.getCharacterList().getItems().get(0).getName())))
        .andReturn().getResponse().getContentAsString();
        assertEquals(testComic, objectMapper.readValue(result, ComicDto.class));
    }
    
    @Test
    void testGetComicByIdShouldReturnReturnNotFoundStatusCodeWhenComicNotFound() throws Exception {
        when(comicService.findComicById(anyInt())).thenThrow(new ResourceNotFoundException("Not found"));
        mockMvc.perform(get("/v1/public/comics/{comicId}", 10))
        .andDo(print())
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$.status", is("NOT_FOUND")))
        .andExpect(jsonPath("$.timestamp", Matchers.notNullValue()))
        .andExpect(jsonPath("$.exception", is("ResourceNotFoundException")))
        .andExpect(jsonPath("$.message", is("Not found")));
    }

    @Test
    void testGetCharactersByComicIdShouldReturnAllCharactersByPassedComicId() throws Exception {
        when(comicService.findCharactersByComicId(
                anyInt(), any(CharacterFilterParameter.class), any(PageRequest.class)))
        .thenReturn(new PageImpl<>(singletonList(testCharacter)));
        String result = mockMvc.perform(get("/v1/public/comics/{comicId}/characters", 10))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(1)))
        .andReturn().getResponse().getContentAsString();
        List<CharacterDto> pageContent = getCharacterPageContent(result);
        assertEquals(testCharacter, pageContent.get(0));
    }

    @Test
    void testUploadImageShouldReturnUpdatedComicWhenImageUploadedSuccessfully() throws Exception {
        MockMultipartFile file = new MockMultipartFile("image.jpg", new byte[10]);
        when(comicService.uploadComicThumbnail(anyInt(), any(MultipartFile.class))).thenReturn(testComic);
        mockMvc.perform(MockMvcRequestBuilders.multipart("/v1/public/comics/{comicId}/thumbnail", 10)
                .file("file", file.getBytes()))
        .andDo(print())
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$", Matchers.notNullValue()));
    }
    
    @Test
    void testUploadImageShouldReturnBadRequestWhenImageNotUploaded() throws Exception {
        MockMultipartFile file = new MockMultipartFile("image.jpg", new byte[10]);
        when(comicService.uploadComicThumbnail(anyInt(), any(MultipartFile.class))).thenThrow(new UploadFileException("message"));
        mockMvc.perform(MockMvcRequestBuilders.multipart("/v1/public/comics/{comicId}/thumbnail", 10)
                .file("file", file.getBytes()))
        .andDo(print())
        .andExpect(status().isBadRequest()); 
    }

    @Test
    void testGetComicThumbnailShouldReturnNotFoundIfThumbnailFileForComicIsNotLoaded() throws Exception {
        when(comicService.getComicThumbnail(anyInt())).thenThrow(new ResourceNotFoundException("message"));
        mockMvc.perform(get("/v1/public/comics/{comicId}/thumbnail", 10))
        .andDo(print())
        .andExpect(status().isNotFound());
    }
    
    @Test
    void testGetComicThumbnailShouldReturnResourceWithThumbnailFileIfFileWasUploaded() throws Exception {
        Path resourcePath = Paths.get(this.getClass().getClassLoader().getResource("testimg.jpg").toURI());
        byte[] expectedContent = Files.readAllBytes(resourcePath);
        when(comicService.getComicThumbnail(anyInt()))
        .thenReturn(new UrlResource(resourcePath.toUri()));
        mockMvc.perform(get("/v1/public/comics/{comicId}/thumbnail", 10))
        .andExpect(status().isOk())
        .andExpect(header().exists("Content-Disposition"))
        .andExpect(content().bytes(expectedContent));
    }

    @Test
    void testSaveComicShouldReturnSavedComicWhenValidRequestPassed() throws Exception {
        when(comicService.saveComic(any(ComicDto.class))).thenReturn(testComic);
        mockMvc.perform(post("/v1/public/comics/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testComic)))
        .andDo(print())
        .andExpect(status().isCreated());
    }
    
    @Test
    void testSaveComicShouldReturnBadRequestWhenNotValidRequestPassed() throws Exception {
        testComic.setTitle("");
        when(comicService.saveComic(any(ComicDto.class))).thenReturn(testComic);
        mockMvc.perform(post("/v1/public/comics/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testComic)))
        .andDo(print())
        .andExpect(status().isBadRequest());
    }
    
    private CharacterDto createTestCharacterDto() {
        CharacterDto characterDto = new CharacterDto();
        characterDto.setId(1);
        characterDto.setName("Spider-man");
        characterDto.setDescription("Payk");
        characterDto.setModified(LocalDate.of(2021, 02, 28));
        characterDto.setThumbnailName("image.jpg");
        characterDto.setComicIds(singletonList(1));
        characterDto.setResourceURI("characterUri");
        ComicList comicList = new ComicList();
        comicList.setAvailable(1);
        comicList.setCollectionURI("uri");
        ComicSummary comicSummary = new ComicSummary();
        comicSummary.setResourceURI("comicUri");
        comicSummary.setTitle("TitleComic");
        comicList.setItems(singletonList(comicSummary));
        characterDto.setComicList(comicList);
        return characterDto;
    }
    
    private ComicDto createTestComicDto() {
        ComicDto comicDto = new ComicDto();
        comicDto.setId(1);
        comicDto.setTitle("TitleComic");
        comicDto.setDescription("Comic description");
        comicDto.setPrice(10);
        comicDto.setPageCount(100);
        comicDto.setThumbnailName("comicimg.jpg");
        comicDto.setStartDate(LocalDate.of(2020, 10, 20));
        comicDto.setModified(LocalDate.of(2020, 10, 20));
        comicDto.setResourceURI("comicUri");
        CharacterList characterList = new CharacterList();
        characterList.setAvailable(1);
        characterList.setCollectionURI("uri");
        CharacterSummary characterSummary = new CharacterSummary();
        characterSummary.setName("Spider-man");
        characterSummary.setResourceURI("characterUri");
        characterList.setItems(singletonList(characterSummary));
        comicDto.setCharacterList(characterList);
        return comicDto;
    }
    
    private List<CharacterDto> getCharacterPageContent(String result) throws Exception {
        return objectMapper.readValue(result, new TypeReference<RestResponsePage<CharacterDto>>() {}).getContent(); 
    }

    private List<ComicDto> getComicPageContent(String result) throws Exception {
        return objectMapper.readValue(result, new TypeReference<RestResponsePage<ComicDto>>() {}).getContent(); 
    }

}
