package com.mymarvel.marvelapi.service;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.exceptions.ResourceNotFoundException;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;
import com.mymarvel.marvelapi.repository.CharacterRepository;
import com.mymarvel.marvelapi.repository.ComicRepository;
import com.mymarvel.marvelapi.service.specifications.CharacterSpecification;
import com.mymarvel.marvelapi.service.specifications.ComicSpecification;
import com.mymarvel.marvelapi.utils.FilesUtil;
import com.mymarvel.marvelapi.utils.mapper.CharacterMapper;
import com.mymarvel.marvelapi.utils.mapper.ComicMapper;

@ExtendWith(MockitoExtension.class)
class ComicServiceImplTest {
    
    @Mock
    private CharacterRepository characterRepository;
    @Mock
    private ComicRepository comicRepository;
    @Mock
    private CharacterMapper characterMapper;
    @Mock
    private ComicMapper comicMapper;
    @Mock
    private FilesUtil filesUtil;
    
    @InjectMocks
    private ComicServiceImpl comicService;

    private Comic testComic;
    private ComicDto testComicDto;
    
    @BeforeEach
    void setUp() throws Exception {
        testComic = new Comic();
        testComicDto = new ComicDto();
    }
    
    @Test
    void testFindAllComicsShouldTrowsIllegalArgumentExceptionWhenPassedFilterParametersIsNull() {
        assertThrows(IllegalArgumentException.class, () -> comicService.findAllComics(null, emptyList(), PageRequest.of(0, 1)));
    }
    
    @Test
    void testFindAllComicsShouldReturnPageWithComicsDtoWhenValidParametersPassed() {
        List<Comic> testComics = Arrays.asList(new Comic(1), new Comic(2), new Comic(3));
        List<ComicDto> expected = Arrays.asList(new ComicDto(), new ComicDto(), new ComicDto());
        when(comicRepository.findAll(any(ComicSpecification.class), any(PageRequest.class))).thenReturn(new PageImpl<>(testComics));
        when(comicMapper.toDto(any(Comic.class))).thenReturn(new ComicDto());
        Page<ComicDto> actual = comicService.findAllComics(new ComicFilterParameter(), emptyList(), PageRequest.of(0, 3));
        assertEquals(expected, actual.getContent());
    }

    @Test
    void testFindComicByIdShouldThrowsResourceNotFoundExceptionWhenComicWithPassedIdDoesNotExist() {
        int comicId = 1;
        when(comicRepository.findById(anyInt())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> comicService.findComicById(comicId));
        assertEquals(String.format("Comic with id = %d doesn't exists", comicId), exception.getMessage());
    }
    
    @Test
    void testFindComicByIdShouldReturnComicDtoWhenComicWithPassedIdIsExists() {
        int comicId = 1;
        when(comicRepository.findById(comicId)).thenReturn(Optional.of(testComic));
        when(comicMapper.toDto(any(Comic.class))).thenReturn(testComicDto);
        assertEquals(new ComicDto(), comicService.findComicById(comicId));
    }

    @Test
    void testFindCharactersByComicIdShouldThrowsIllegalArgumentExceptionWhenPassedFilterParametersIsNull() {
        assertThrows(IllegalArgumentException.class, () -> comicService.findCharactersByComicId(10, null, PageRequest.of(0, 1)));
    }
    
    @Test
    void testFindCharactersByComicIdShouldReturnEmptyPageWhenComicWithPassedIdDoesNotExist() {
        int comicId = 1;
        when(characterRepository.findAll(any(CharacterSpecification.class), any(PageRequest.class))).thenReturn(new PageImpl<>(emptyList()));
        assertEquals(emptyList(), comicService.findCharactersByComicId(comicId, new CharacterFilterParameter(), PageRequest.of(0, 1)).getContent());
    }
    
    @Test
    void testFindCharactersByComicIdShouldReturnCharactersPageWhenComicWithPassedIdExists() {
        int comicId = 1;
        List<Character> testCharacters = Arrays.asList(new Character(1), new Character(2), new Character(3));
        List<CharacterDto> expected = Arrays.asList(new CharacterDto(), new CharacterDto(), new CharacterDto());
        when(characterRepository.findAll(any(CharacterSpecification.class), any(PageRequest.class))).thenReturn(new PageImpl<>(testCharacters));
        when(characterMapper.toDto(any(Character.class))).thenReturn(new CharacterDto());
        Page<CharacterDto> actual = comicService.findCharactersByComicId(comicId, new CharacterFilterParameter(), PageRequest.of(0, 3));
        assertEquals(expected, actual.getContent());
    }

    @Test
    void testSaveComicShouldThrowsIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> comicService.saveComic(null));
    }
    
    @Test
    void testSaveComicShouldReturnSavedComicDtoWhenComicDtoPassed() {
        when(comicMapper.toEntity(any(ComicDto.class))).thenReturn(testComic);
        when(comicRepository.save(any(Comic.class))).thenReturn(testComic);
        when(comicMapper.toDto(any(Comic.class))).thenReturn(testComicDto);
        assertEquals(testComicDto, comicService.saveComic(testComicDto));
    }

    @Test
    void testUploadComicThumbnailShouldThrowsIllegalArgumentExceptionWhenNullFilePassed() {
        assertThrows(IllegalArgumentException.class, () -> comicService.uploadComicThumbnail(1, null));
    }
    
    @Test
    void testUploadComicThumbnailShouldReturnUpdatedComicDtoWhenComicIdAndFilePassed() {
        String fileName = "FileName.jpg";
        when(comicRepository.findById(anyInt())).thenReturn(Optional.of(testComic));
        when(comicMapper.toDto(testComic)).thenReturn(testComicDto);
        when(filesUtil.uploadFile(any(MultipartFile.class))).thenReturn(fileName);
        when(comicRepository.save(any(Comic.class))).thenReturn(testComic);
        when(comicMapper.toEntity(any(ComicDto.class))).thenReturn(testComic);
        ComicDto actual = comicService.uploadComicThumbnail(0, new MockMultipartFile("filename", new byte[10]));
        testComicDto.setThumbnailName(fileName);
        ComicDto expected = testComicDto;
        assertEquals(expected, actual);
    }

    @Test
    void testGetComicThumbnailShouldThrowsResourceNotFoundExceptionWhenComicThumbnailNameIsNull() {
        int comicId = 1;
        when(comicRepository.findById(anyInt())).thenReturn(Optional.of(testComic));
        when(comicMapper.toDto(any(Comic.class))).thenReturn(testComicDto);
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> comicService.getComicThumbnail(comicId));
        assertEquals(String.format("The thumbnail for the comic with id = %d has not been loaded yet", comicId), exception.getMessage()); 
    }
    
    @Test
    void testGetComicThumbnailShouldReturnResourceWithComicThumbnailFile() throws MalformedURLException {
        String thumbnailName = "image.jpg";
        testComicDto.setThumbnailName(thumbnailName);
        int comicId = 1;
        when(comicRepository.findById(anyInt())).thenReturn(Optional.of(testComic));
        when(comicMapper.toDto(any(Comic.class))).thenReturn(testComicDto);
        when(filesUtil.loadFileAsResource(thumbnailName)).thenReturn(new UrlResource(Paths.get(thumbnailName).toUri()));
         assertEquals(thumbnailName, comicService.getComicThumbnail(comicId).getFilename());
    }
}
