package com.mymarvel.marvelapi.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static java.util.Collections.emptyList;
import static org.mockito.Mockito.*;

import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.mymarvel.marvelapi.controllers.parameters.CharacterFilterParameter;
import com.mymarvel.marvelapi.controllers.parameters.ComicFilterParameter;
import com.mymarvel.marvelapi.exceptions.ResourceNotFoundException;
import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;
import com.mymarvel.marvelapi.repository.CharacterRepository;
import com.mymarvel.marvelapi.repository.ComicRepository;
import com.mymarvel.marvelapi.service.specifications.CharacterSpecification;
import com.mymarvel.marvelapi.service.specifications.ComicSpecification;
import com.mymarvel.marvelapi.utils.FilesUtil;
import com.mymarvel.marvelapi.utils.mapper.CharacterMapper;
import com.mymarvel.marvelapi.utils.mapper.ComicMapper;

@ExtendWith(MockitoExtension.class)
class CharacterServiceImplTest {
    
    @Mock
    private CharacterRepository characterRepository;
    @Mock
    private ComicRepository comicRepository;
    @Mock
    private CharacterMapper characterMapper;
    @Mock
    private ComicMapper comicMapper;
    @Mock
    private FilesUtil filesUtil;
    
    @InjectMocks
    private CharacterServiceImpl characterService;
    
    private Character testCharacter;
    private CharacterDto testCharacterDto;

    @BeforeEach
    void setUp() throws Exception {
        testCharacter = new Character();
        testCharacterDto = new CharacterDto();
    }

    @Test
    void testFindAllCharactersShouldTrowsIllegalArgumentExceptionWhenPassedFilterParametersIsNull() {
        assertThrows(IllegalArgumentException.class, () -> characterService.findAllCharacters(null, emptyList(), PageRequest.of(0, 1)));
    }
    
    @Test
    void testFindAllCharactersShouldReturnPageWithCharactersDtoWhenValidParametersPassed() {
        List<Character> testCharacters = Arrays.asList(new Character(1), new Character(2), new Character(3));
        List<CharacterDto> expected = Arrays.asList(new CharacterDto(), new CharacterDto(), new CharacterDto());
        when(characterRepository.findAll(any(CharacterSpecification.class), any(PageRequest.class))).thenReturn(new PageImpl<>(testCharacters));
        when(characterMapper.toDto(any(Character.class))).thenReturn(new CharacterDto());
        Page<CharacterDto> actual = characterService.findAllCharacters(new CharacterFilterParameter(), emptyList(), PageRequest.of(0, 3));
        assertEquals(expected, actual.getContent());
    }

    @Test
    void testFindCharacterByIdShouldThrowsResourceNotFoundExceptionWhenCharacterWithPassedIdDoesNotExist() {
        int characterId = 1;
        when(characterRepository.findById(anyInt())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> characterService.findCharacterById(characterId));
        assertEquals(String.format("Character with id = %d doesn't exists", characterId), exception.getMessage());
    }
    
    @Test
    void testFindCharacterByIdShouldReturnCharacterDtoWhenCharacterWithPassedIdIsExists() {
        int characterId = 1;
        when(characterRepository.findById(characterId)).thenReturn(Optional.of(testCharacter));
        when(characterMapper.toDto(any(Character.class))).thenReturn(testCharacterDto);
        assertEquals(new CharacterDto(), characterService.findCharacterById(characterId));
    }

    @Test
    void testFindComicsByCharacterIdShouldThrowsIllegalArgumentExceptionWhenPassedFilterParametersIsNull() {
        assertThrows(IllegalArgumentException.class, () -> characterService.findComicsByCharacterId(10, null, PageRequest.of(0, 1)));
    }
    
    @Test
    void testFindComicsByCharacterIdShouldReturnEmptyPageWhenCharacterWithPassedIdDoesNotExist() {
        int characterId = 1;
        when(comicRepository.findAll(any(ComicSpecification.class), any(PageRequest.class))).thenReturn(new PageImpl<>(emptyList()));
        assertEquals(emptyList(), characterService.findComicsByCharacterId(characterId, new ComicFilterParameter(), PageRequest.of(0, 1)).getContent());
    }
    
    @Test
    void testFindComicsByCharacterIdShouldReturnComicsPageWhenCharacterWithPassedIdExists() {
        int characterId = 1;
        List<Comic> testComics = Arrays.asList(new Comic(1), new Comic(2), new Comic(3));
        List<ComicDto> expected = Arrays.asList(new ComicDto(), new ComicDto(), new ComicDto());
        when(comicRepository.findAll(any(ComicSpecification.class), any(PageRequest.class))).thenReturn(new PageImpl<>(testComics));
        when(comicMapper.toDto(any(Comic.class))).thenReturn(new ComicDto());
        Page<ComicDto> actual = characterService.findComicsByCharacterId(characterId, new ComicFilterParameter(), PageRequest.of(0, 3));
        assertEquals(expected, actual.getContent());
    }

    @Test
    void testSaveCharacterShouldThrowsIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> characterService.saveCharacter(null));
    }
    
    @Test
    void testSaveCharacterShouldReturnSavedCharacterDtoWhenCharacterDtoPassed() {
        when(characterMapper.toEntity(any(CharacterDto.class))).thenReturn(testCharacter);
        when(characterRepository.save(any(Character.class))).thenReturn(testCharacter);
        when(characterMapper.toDto(any(Character.class))).thenReturn(testCharacterDto);
        assertEquals(testCharacterDto, characterService.saveCharacter(testCharacterDto));
    }

    @Test
    void testUploadCharacterThumbnailShouldThrowsIllegalArgumentExceptionWhenNullFilePassed() {
        assertThrows(IllegalArgumentException.class, () -> characterService.uploadCharacterThumbnail(1, null));
    }
    
    @Test
    void testUploadCharacterThumbnailShouldReturnUpdatedCharacterDtoWhenCharacterIdAndFilePassed() {
        String fileName = "FileName.jpg";
        when(characterRepository.findById(anyInt())).thenReturn(Optional.of(testCharacter));
        when(characterMapper.toDto(testCharacter)).thenReturn(testCharacterDto);
        when(filesUtil.uploadFile(any(MultipartFile.class))).thenReturn(fileName);
        when(characterRepository.save(any(Character.class))).thenReturn(testCharacter);
        when(characterMapper.toEntity(any(CharacterDto.class))).thenReturn(testCharacter);
        CharacterDto actual = characterService.uploadCharacterThumbnail(0, new MockMultipartFile("filename", new byte[10]));
        testCharacterDto.setThumbnailName(fileName);
        CharacterDto expected = testCharacterDto;
        assertEquals(expected, actual);
    }

    @Test
    void testGetCharacterThumbnailShouldThrowsResourceNotFoundExceptionWhenCharacterThumbnailNameIsNull() {
        int characterId = 1;
        when(characterRepository.findById(anyInt())).thenReturn(Optional.of(testCharacter));
        when(characterMapper.toDto(any(Character.class))).thenReturn(testCharacterDto);
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> characterService.getCharacterThumbnail(characterId));
        assertEquals(String.format("The thumbnail for the character with id = %d has not been loaded yet", characterId), exception.getMessage()); 
    }
    
    @Test
    void testGetCharacterThumbnailShouldReturnResourceWithCharacterThumbnailFile() throws MalformedURLException {
        String thumbnailName = "image.jpg";
        testCharacterDto.setThumbnailName(thumbnailName);
        int characterId = 1;
        when(characterRepository.findById(anyInt())).thenReturn(Optional.of(testCharacter));
        when(characterMapper.toDto(any(Character.class))).thenReturn(testCharacterDto);
        when(filesUtil.loadFileAsResource(thumbnailName)).thenReturn(new UrlResource(Paths.get(thumbnailName).toUri()));
         assertEquals(thumbnailName, characterService.getCharacterThumbnail(characterId).getFilename());
    }
}
