package com.mymarvel.marvelapi.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;

import com.mymarvel.marvelapi.exceptions.ResourceNotFoundException;
import com.mymarvel.marvelapi.exceptions.UploadFileException;

class FilesUtilTest {
    
    private static final String IMAGE_FILE = "testimg.jpg";
    private static final String NO_IMAGE_FILE = "noimg.png";

    @TempDir
    Path testDirPath;
    private FilesUtil filesUtil;
    private MockMultipartFile multipartFile;
    
    @BeforeEach
    void setUp() throws Exception {
        filesUtil = new FilesUtil();
        ReflectionTestUtils.setField(filesUtil, "uploadDir", testDirPath.toString());
    }

    @Test
    void testUploadFileShouldReturnNewFileNameWithUUIDWhenMultipartFileWithImageMediaTypePassed() throws Exception {
        multipartFile = new MockMultipartFile(IMAGE_FILE, IMAGE_FILE, "image/jpg", readResourceFile(IMAGE_FILE));
        String actual = filesUtil.uploadFile(multipartFile);
        assertTrue(actual.contains(IMAGE_FILE));
    }
    
    @Test
    void testUploadFileShouldThrowsUploadFileExceptionWhenMultipartFileWithNonImageMediaTypePassed() throws Exception {
        multipartFile = new MockMultipartFile(NO_IMAGE_FILE, NO_IMAGE_FILE, "image/png", readResourceFile(NO_IMAGE_FILE));
        Throwable exception = assertThrows(UploadFileException.class, () -> filesUtil.uploadFile(multipartFile));
        assertTrue(exception.getMessage().contains("The file must be an image. Media type of current file:"));
    }
    
    @Test
    void testUploadFileShouldThrowsIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> filesUtil.uploadFile(null));
    }
    
    @Test
    void testLoadFileAsResourceShouldThrowsIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> filesUtil.loadFileAsResource(null));
    }
    
    @Test
    void testLoadFileAsResourceShouldThrowsResourceNotFoundExceptionWhenNonExistingFileNamePassed() {
        assertThrows(ResourceNotFoundException.class, () -> filesUtil.loadFileAsResource("nonexistingfile.jpg"));
    }
    
    @Test
    void testLoadFileAsResourceShouldReturnResourceWithFileWhenExistingFileNamePassed() throws Exception {
        Path testPath = testDirPath.resolve(IMAGE_FILE);
        Files.write(testPath, readResourceFile(IMAGE_FILE), StandardOpenOption.CREATE);
        assertEquals(IMAGE_FILE, filesUtil.loadFileAsResource(IMAGE_FILE).getFile().getName());
    }
    
    private byte[] readResourceFile(String pathOnClassPath) throws Exception {
        return Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource(pathOnClassPath).toURI()));
     }
}
