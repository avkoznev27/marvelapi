package com.mymarvel.marvelapi.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;

class ResourceUriBuilderTest {
    
    private ResourceUriBuilder uriBuilder;
    private Character testCharacter;
    private Comic testComic;

    @BeforeEach
    void setUp() throws Exception {
        uriBuilder = new ResourceUriBuilder();
        ReflectionTestUtils.setField(uriBuilder, "charactersApiBasePath", "/test/v1/characters");
        ReflectionTestUtils.setField(uriBuilder, "comicsApiBasePath", "/test/v1/comics");
        testCharacter = new Character(10);
        testComic  = new Comic(15);
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    void testGetCharacterUriShouldReturnUriForPassedCharacterWithCurrentContextPath() {
        String uriExpected = "http://localhost/test/v1/characters/10";
        String uriActual = uriBuilder.getCharacterUri(testCharacter);
        assertEquals(uriExpected, uriActual);
    }
    
    @Test
    void testGetCharacterUriShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> uriBuilder.getCharacterUri(null));
    }

    @Test
    void testGetCharacterComicsUriShouldReturnUriForComicsPassedCharacterWithCurrentContextPath() {
        String uriExpected = "http://localhost/test/v1/characters/10/comics";
        String uriActual = uriBuilder.getCharacterComicsUri(testCharacter);
        assertEquals(uriExpected, uriActual);
    }
    
    @Test
    void testGetCharacterComicsUriShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> uriBuilder.getCharacterComicsUri(null));
    }

    @Test
    void testGetComicUriShouldReturnUriForPassedComicWithCurrentContextPath() {
        String uriExpected = "http://localhost/test/v1/comics/15";
        String uriActual = uriBuilder.getComicUri(testComic);
        assertEquals(uriExpected, uriActual);
    }
    
    @Test
    void testGetComicUriShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> uriBuilder.getComicUri(null));
    }

    @Test
    void testGetComicCharactersUriShouldReturnUriForCharactersPassedComicWithCurrentContextPath() {
        String uriExpected = "http://localhost/test/v1/comics/15/characters";
        String uriActual = uriBuilder.getComicCharactersUri(testComic);
        assertEquals(uriExpected, uriActual);
    }
    
    @Test
    void testGetComicCharactersUriShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> uriBuilder.getComicCharactersUri(null));
    }

}
