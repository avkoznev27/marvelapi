package com.mymarvel.marvelapi.utils.mapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.Collections.emptyList;

import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.mymarvel.marvelapi.model.dto.CharacterList;
import com.mymarvel.marvelapi.model.dto.CharacterSummary;
import com.mymarvel.marvelapi.model.dto.ComicDto;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;
import com.mymarvel.marvelapi.repository.CharacterRepository;
import com.mymarvel.marvelapi.utils.ResourceUriBuilder;

@ExtendWith(MockitoExtension.class)
class ComicMapperTest { 
    
    @Mock
    private ModelMapper modelMapper;
    @Mock
    private ResourceUriBuilder uriBuilder;
    @Mock
    private CharacterRepository characterRepository;
    
    @InjectMocks
    private ComicMapper comicMapper;
    
    private Character testCharacter;
    private Comic testComic;
    private ComicDto testComicDto;
    
    @BeforeEach
    void setUp() throws Exception {
        testComic = createTestComic();
        testCharacter = createTestCharacter();
        testComicDto = createTestComicDto();
    }

    @Test
    void testToEntityShouldThrowsIllegalArgumebtExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> comicMapper.toEntity(null));
    }
    
    @Test
    void testToEntityShouldReturnComicEntityWhenComicDtoPassed() {
        when(modelMapper.map(testComicDto, Comic.class)).thenReturn(testComic);
        when(characterRepository.findAllById(testComicDto.getCharacterIds())).thenReturn(singletonList(testCharacter));
        Comic actualComic = comicMapper.toEntity(testComicDto);
        testComic.setCharacters(singleton(testCharacter));
        assertEquals(testComic, actualComic);
        assertEquals(testComic.getCharacters(), actualComic.getCharacters());
    }
    
    @Test
    void testToEntityShouldReturnComicEntityWithNullValueFieldsWhenComicDtoWithNullValueFieldPassed() {
        Comic emptyComic = new Comic();
        ComicDto emptyComicDto = new ComicDto();
        when(modelMapper.map(emptyComicDto, Comic.class)).thenReturn(emptyComic);
        when(characterRepository.findAllById(emptyComicDto.getCharacterIds())).thenReturn(emptyList());
        assertEquals(emptyComic, comicMapper.toEntity(emptyComicDto));
    }

    @Test
    void testToDtoShouldThrowsIllegalArgumebtExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> comicMapper.toDto(null));
    }
    
    @Test
    void testToDtoShouldReturnComicDtoWhenComicEntityPassed() {
        when(modelMapper.map(testComic, ComicDto.class)).thenReturn(testComicDto);
        when(uriBuilder.getComicUri(testComic)).thenReturn("comicUri");
        when(uriBuilder.getComicCharactersUri(testComic)).thenReturn("uri");
        when(uriBuilder.getCharacterUri(testCharacter)).thenReturn("characterUri");
        testComic.setCharacters(singleton(testCharacter));
        ComicDto actualComicDto = comicMapper.toDto(testComic);
        testComicDto.setCharacterList(createTestCharacterList());
        assertEquals(testComicDto, actualComicDto);
        assertEquals(testComicDto.getCharacterIds(), actualComicDto.getCharacterIds());
        assertEquals(testComicDto.getCharacterList(), actualComicDto.getCharacterList());
    }
    
    @Test
    void testToDtoShouldReturnComicDtoWithNullValueFieldsWhenComicEntityWithNullValueFieldPassed() {
        Comic emptyComic = new Comic();
        ComicDto emptyComicDto = new ComicDto();
        when(modelMapper.map(emptyComic, ComicDto.class)).thenReturn(emptyComicDto);
        when(uriBuilder.getComicUri(emptyComic)).thenReturn("comicUri");
        when(uriBuilder.getComicCharactersUri(emptyComic)).thenReturn("uri");
        assertEquals(emptyComicDto, comicMapper.toDto(emptyComic));
    }
    
    private ComicDto createTestComicDto() {
        ComicDto comicDto = new ComicDto();
        comicDto.setId(1);
        comicDto.setTitle("TitleComic");
        comicDto.setDescription("Comic description");
        comicDto.setPrice(10);
        comicDto.setPageCount(100);
        comicDto.setThumbnailName("comicimg.jpg");
        comicDto.setModified(LocalDate.of(2020, 10, 20));
        comicDto.setResourceURI("comicUri");
        return comicDto;
    }
    
    private CharacterList createTestCharacterList() {
        CharacterList characterList = new CharacterList();
        characterList.setAvailable(1);
        characterList.setCollectionURI("uri");
        CharacterSummary characterSummary = new CharacterSummary();
        characterSummary.setName("Spider-man");
        characterSummary.setResourceURI("characterUri");
        characterList.setItems(singletonList(characterSummary));
        return characterList;
                
    }

    private Character createTestCharacter() {
        Character character = new Character(1);
        character.setName("Spider-man");
        character.setDescription("Payk");
        character.setModified(LocalDate.of(2021, 02, 28));
        character.setThumbnailName("image.jpg");
        return character;
    }
    
    private Comic createTestComic() {
        Comic comic = new Comic(1);
        comic.setTitle("TitleComic");
        comic.setDescription("Comic description");
        comic.setPrice(10);
        comic.setPageCount(100);
        comic.setThumbnailName("comicimg.jpg");
        comic.setModified(LocalDate.of(2020, 10, 20));
        return comic;
    }

}
