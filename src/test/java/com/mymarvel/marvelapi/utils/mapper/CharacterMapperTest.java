package com.mymarvel.marvelapi.utils.mapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.Collections.emptyList;

import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.mymarvel.marvelapi.model.dto.CharacterDto;
import com.mymarvel.marvelapi.model.dto.ComicList;
import com.mymarvel.marvelapi.model.dto.ComicSummary;
import com.mymarvel.marvelapi.model.entities.Character;
import com.mymarvel.marvelapi.model.entities.Comic;
import com.mymarvel.marvelapi.repository.ComicRepository;
import com.mymarvel.marvelapi.utils.ResourceUriBuilder;

@ExtendWith(MockitoExtension.class)
class CharacterMapperTest {
    
    @Mock
    private ModelMapper modelMapper;
    @Mock
    private ResourceUriBuilder uriBuilder;
    @Mock
    private ComicRepository comicRepository;
    
    @InjectMocks
    private CharacterMapper characterMapper;
    
    private Character testCharacter;
    private Comic testComic;
    private CharacterDto testCharacterDto;


    @BeforeEach
    void setUp() throws Exception {
        testCharacter = createTestCharacter();
        testComic = createTestComic();
        testCharacterDto = createTestCharacterDto();
    }

    @Test
    void testToEntityShouldThrowsIllegalArgumebtExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> characterMapper.toEntity(null));
    }
    
    @Test
    void testToEntityShouldReturnCharacterEntityWhenCharacterDtoPassed() {
        when(modelMapper.map(testCharacterDto, Character.class)).thenReturn(testCharacter);
        when(comicRepository.findAllById(testCharacterDto.getComicIds())).thenReturn(singletonList(testComic));
        Character actualCharacter = characterMapper.toEntity(testCharacterDto);
        testCharacter.setComics(singleton(testComic));
        assertEquals(testCharacter, actualCharacter);
        assertEquals(testCharacter.getComics(), actualCharacter.getComics());
    }
    
    @Test
    void testToEntityShouldReturnCharacterEntityWithNullValueFieldsWhenCharacterDtoWithNullValueFieldPassed() {
        Character emptyCharacter = new Character();
        CharacterDto emptyCharacterDto = new CharacterDto();
        when(modelMapper.map(emptyCharacterDto, Character.class)).thenReturn(emptyCharacter);
        when(comicRepository.findAllById(emptyCharacterDto.getComicIds())).thenReturn(emptyList());
        assertEquals(emptyCharacter, characterMapper.toEntity(emptyCharacterDto));
    }
    
    @Test
    void testToDtoShouldThrowsIllegalArgumebtExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> characterMapper.toDto(null));
    }
    
    @Test
    void testToDtoShouldReturnCharcterDtoWhenCharacterEntityPassed() {
        when(modelMapper.map(testCharacter, CharacterDto.class)).thenReturn(testCharacterDto);
        when(uriBuilder.getCharacterUri(testCharacter)).thenReturn("characterUri");
        when(uriBuilder.getComicUri(testComic)).thenReturn("comicUri");
        when(uriBuilder.getCharacterComicsUri(testCharacter)).thenReturn("uri");
        testCharacter.setComics(singleton(testComic));
        CharacterDto actual = characterMapper.toDto(testCharacter);
        testCharacterDto.setComicList(createTestComicList());
        assertEquals(testCharacterDto, actual);
        assertEquals(testCharacterDto.getComicIds(), actual.getComicIds());
        assertEquals(testCharacterDto.getComicList(), actual.getComicList());
    }
    
    @Test
    void testToDtoShouldReturnCharacterDtoWithNullValueFieldsWhenCharacterEntityWithNullValueFieldPassed() {
        Character emptyCharacter = new Character();
        CharacterDto emptyCharacterDto = new CharacterDto();
        when(modelMapper.map(emptyCharacter, CharacterDto.class)).thenReturn(emptyCharacterDto);
        when(uriBuilder.getCharacterUri(emptyCharacter)).thenReturn("characterUri");
        when(uriBuilder.getCharacterComicsUri(emptyCharacter)).thenReturn("uri");
        assertEquals(emptyCharacterDto, characterMapper.toDto(emptyCharacter));
    }
    
    private CharacterDto createTestCharacterDto() {
        CharacterDto characterDto = new CharacterDto();
        characterDto.setId(1);
        characterDto.setName("Spider-man");
        characterDto.setDescription("Payk");
        characterDto.setModified(LocalDate.of(2021, 02, 28));
        characterDto.setThumbnailName("image.jpg");
        characterDto.setComicIds(singletonList(1));
        characterDto.setResourceURI("characterUri");
        return characterDto;
    }
    
    private ComicList createTestComicList() {
        ComicList comicList = new ComicList();
        comicList.setAvailable(1);
        comicList.setCollectionURI("uri");
        ComicSummary comicSummary = new ComicSummary();
        comicSummary.setResourceURI("comicUri");
        comicSummary.setTitle("TitleComic");
        comicList.setItems(singletonList(comicSummary));
        return comicList;
    }

    private Character createTestCharacter() {
        Character character = new Character(1);
        character.setName("Spider-man");
        character.setDescription("Payk");
        character.setModified(LocalDate.of(2021, 02, 28));
        character.setThumbnailName("image.jpg");
        return character;
    }
    
    private Comic createTestComic() {
        Comic comic = new Comic(1);
        comic.setTitle("TitleComic");
        comic.setDescription("Comic description");
        comic.setPrice(10);
        comic.setPageCount(100);
        comic.setThumbnailName("comicimg.jpg");
        comic.setModified(LocalDate.of(2020, 10, 20));
        return comic;
    }
}
